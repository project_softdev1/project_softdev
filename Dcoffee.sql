--
-- File generated with SQLiteStudio v3.4.4 on �. �.�. 9 14:49:46 2023
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: category
CREATE TABLE IF NOT EXISTS category (category_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, category_name TEXT UNIQUE NOT NULL);

-- Table: check_stock
CREATE TABLE IF NOT EXISTS check_stock (check_stock_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, check_stock_datetime DATETIME NOT NULL, employee_id INTEGER REFERENCES employee (employee_id) ON DELETE RESTRICT ON UPDATE CASCADE);

-- Table: check_stock_detail
CREATE TABLE IF NOT EXISTS check_stock_detail (check_stock_detail_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, check_stock_id INTEGER REFERENCES check_stock (check_stock_id) ON DELETE RESTRICT ON UPDATE CASCADE, ingredient_id INTEGER REFERENCES ingredient (ingredient_id) ON DELETE RESTRICT ON UPDATE CASCADE, check_stock_detail_datetime DATETIME NOT NULL);

-- Table: checkinout
CREATE TABLE IF NOT EXISTS checkinout (checkinout_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, checkinout_indatetime DATETIME, checkinout_outdatetime DATETIME, employee_id INTEGER REFERENCES employee (employee_id) ON DELETE RESTRICT ON UPDATE CASCADE, salary_id INTEGER REFERENCES salary (salary_id) ON DELETE RESTRICT ON UPDATE CASCADE);

-- Table: customer
CREATE TABLE IF NOT EXISTS customer (customer_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, customer_name TEXT NOT NULL, customer_tel TEXT UNIQUE NOT NULL);

-- Table: employee
CREATE TABLE IF NOT EXISTS employee (employee_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, employee_name TEXT NOT NULL, employee_tel UNIQUE NOT NULL, employee_role TEXT NOT NULL, employee_login TEXT UNIQUE, employee_password TEXT NOT NULL);

-- Table: ingredient
CREATE TABLE IF NOT EXISTS ingredient (ingredient_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, ingredient_name TEXT NOT NULL, ingredient_qoh INTEGER NOT NULL, ingredient_price REAL NOT NULL);

-- Table: ingredient_receipt
CREATE TABLE IF NOT EXISTS ingredient_receipt (ingredient_receipt_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, ingredient_receipt_datetime DATETIME NOT NULL, ingredient_receipt_total REAL NOT NULL, ingredient_receipt_vender TEXT NOT NULL, employee_id INTEGER REFERENCES employee (employee_id) ON DELETE RESTRICT ON UPDATE CASCADE NOT NULL);

-- Table: ingredient_receipt_detail
CREATE TABLE IF NOT EXISTS ingredient_receipt_detail (ingredient_receipt_detail_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, ingredient_receipt_id INTEGER REFERENCES ingredient_receipt (ingredient_receipt_id) ON DELETE RESTRICT ON UPDATE CASCADE, ingredient_id INTEGER REFERENCES ingredient (ingredient_id) ON DELETE RESTRICT ON UPDATE CASCADE, ingredient_name TEXT NOT NULL, ingredient_price REAL NOT NULL, ingredient_qty INTEGER NOT NULL, ingredient_total REAL NOT NULL);

-- Table: owner
CREATE TABLE IF NOT EXISTS owner (owner_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, owner_name TEXT NOT NULL, owner_tel TEXT NOT NULL UNIQUE);

-- Table: product
CREATE TABLE IF NOT EXISTS product (product_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, product_name TEXT NOT NULL, product_price REAL NOT NULL, product_qty INTEGER NOT NULL, product_size TEXT DEFAULT SML, product_type TEXT DEFAULT HCF, category_id INTEGER REFERENCES category (category_id) ON DELETE RESTRICT ON UPDATE CASCADE);

-- Table: promotion
CREATE TABLE IF NOT EXISTS promotion (promotion_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, promotion_name TEXT NOT NULL, promotion_description TEXT NOT NULL, promotion_startdate DATETIME NOT NULL, promotion_enddate DATETIME NOT NULL);

-- Table: receipt
CREATE TABLE IF NOT EXISTS receipt (receipt_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, receipt_datetime DATETIME NOT NULL, receipt_total REAL NOT NULL, receipt_cash REAL, receipt_qty INTEGER, employee_id INTEGER REFERENCES employee (employee_id) ON DELETE RESTRICT ON UPDATE CASCADE NOT NULL, customer_id INTEGER REFERENCES customer (customer_id) ON DELETE RESTRICT ON UPDATE CASCADE, promotion_id INTEGER REFERENCES promotion (promotion_id) ON DELETE RESTRICT ON UPDATE CASCADE);

-- Table: receipt_detail
CREATE TABLE IF NOT EXISTS receipt_detail (receipt_detail_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, product_id INTEGER REFERENCES product (product_id) ON DELETE RESTRICT ON UPDATE CASCADE NOT NULL, product_name TEXT NOT NULL, product_price REAL NOT NULL, product_qty INTEGER NOT NULL, product_total REAL NOT NULL, receipt_id INTEGER REFERENCES receipt (receipt_id) ON DELETE RESTRICT ON UPDATE CASCADE);

-- Table: salary
CREATE TABLE IF NOT EXISTS salary (salary_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, employee_id INTEGER REFERENCES employee (employee_id) ON DELETE RESTRICT ON UPDATE CASCADE, owner_id INTEGER REFERENCES owner (owner_id) ON DELETE RESTRICT ON UPDATE CASCADE);

-- Table: store_outcome
CREATE TABLE IF NOT EXISTS store_outcome (store_outcome_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, owner_id INTEGER REFERENCES owner (owner_id) ON DELETE RESTRICT ON UPDATE CASCADE, store_outcome_datetime DATETIME NOT NULL, store_outcome_total REAL NOT NULL);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
