/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.CheckInOut;

/**
 *
 * @author Gift
 */
public class CheckInOutDao {

    public CheckInOut save(CheckInOut obj) {
        String sql = "INSERT INTO checkinout (checkinout_outdatetime ,employee_id, salary_id)"
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getCheckOutDate());
            stmt.setInt(2, obj.getEmployeeId());
            stmt.setInt(3, obj.getSalaryId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    public CheckInOut update(CheckInOut obj) {
        String sql = "UPDATE checkinout"
                + " SET checkinout_outdatetime = ? , employee_id = ?, salary_id = ?"
                + " WHERE checkinout_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getCheckOutDate());
            stmt.setInt(2, obj.getEmployeeId());
            stmt.setInt(3, obj.getSalaryId());
            stmt.setInt(4, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public int delete(CheckInOut obj) {
        String sql = "DELETE FROM checkinout WHERE checkinout_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public List<CheckInOut> getAll() {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM checkinout";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                CheckInOut employee = CheckInOut.fromRS(rs);
                list.add(employee);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    //get all + where + order by
    public List<CheckInOut> getAll(String where, String order) {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM checkinout where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut employee = CheckInOut.fromRS(rs);
                list.add(employee);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    // get all + order by
    public List<CheckInOut> getAll(String order) {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM checkinout  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut employee = CheckInOut.fromRS(rs);
                list.add(employee);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public CheckInOut getByEmployeeId(int employeeId) {
        CheckInOut checkinout = null;
        String sql = "SELECT * FROM checkinout WHERE employee_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, employeeId);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                checkinout = CheckInOut.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkinout;
    }

}
