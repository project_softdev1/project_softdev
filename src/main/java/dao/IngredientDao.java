/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Ingredient;

/**
 *
 * @author paewnosuke
 */
public class IngredientDao {
       
    public Ingredient get(int id){
        Ingredient ingredient = null;
        String sql = "SELECT * FROM ingredient WHERE ingredient_id=?"; 
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            
            while (rs.next()) {
                ingredient = Ingredient.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return ingredient;
        }
    
    public List<Ingredient> getAll(){
        ArrayList<Ingredient> list = new ArrayList();
        String sql = "SELECT * FROM ingredient";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Ingredient ingredient = Ingredient.fromRS(rs);
                list.add(ingredient);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Ingredient> getAll(String where, String order){
        ArrayList<Ingredient> list = new ArrayList();
        String sql = "SELECT * FROM ingredient where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while (rs.next()){
                Ingredient ingredient = Ingredient.fromRS(rs);
                list.add(ingredient);
            }
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Ingredient> getAll(String order){
        ArrayList<Ingredient> list = new ArrayList();
        String sql = "SELECT * FROM ingredient ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while (rs.next()){
                Ingredient ingredient = Ingredient.fromRS(rs);
                list.add(ingredient);
            }
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return list;
        
}
    public Ingredient save(Ingredient obj){
        String sql = "INSERT INTO ingredient (ingredient_name, ingredient_qty, ingredient_price)" 
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getQty());
            stmt.setFloat(3, obj.getPrice());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    public Ingredient update(Ingredient obj) {
        String sql = "UPDATE ingredient"
                + " SET ingredient_name = ?, ingredient_qty = ?,ingredient_price = ? "
                + " WHERE ingredient_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getQty());
            stmt.setFloat(3, obj.getPrice());
            stmt.setInt(4, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public int delete(Ingredient obj) {
        String sql = "DELETE FROM ingredient WHERE ingredient_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    
}
