/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.IngredientReciept;

/**
 *
 * @author Asus Vivobook
 */
public class IngredientRecieptDao implements Dao<IngredientReciept> {

    @Override
    public IngredientReciept get(int id) {
        IngredientReciept ingredientReciept = null;
        String sql = "SELECT * FROM ingredient_receipt WHERE ingredient_receipt_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ingredientReciept = IngredientReciept.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return ingredientReciept;
    }

    @Override
    public List<IngredientReciept> getAll() {
        ArrayList<IngredientReciept> list = new ArrayList();
        String sql = "SELECT * FROM ingredient_receipt";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                IngredientReciept ingredient_reciept = IngredientReciept.fromRS(rs);
                list.add(ingredient_reciept);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<IngredientReciept> getAll(String order) {
        ArrayList<IngredientReciept> list = new ArrayList();
        String sql = "SELECT * FROM ingredient_receipt ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                IngredientReciept ingredient_reciept = IngredientReciept.fromRS(rs);
                list.add(ingredient_reciept);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public IngredientReciept save(IngredientReciept obj) {
        String sql = "INSERT INTO ingredient_receipt (ingredient_receipt_total, ingredient_receipt_vendor,employee_id)"
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotal());
            stmt.setString(2, obj.getVendor());
            stmt.setInt(3, obj.getEmployeeId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public IngredientReciept update(IngredientReciept obj) {
        String sql = "UPDATE ingredient_receipt"
                + " SET ingredient_receipt_total = ?, ingredient_receipt_vendor = ?, employee_id = ? "
                + " WHERE ingredient_receipt_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotal());
            stmt.setString(2, obj.getVendor());
            stmt.setInt(3, obj.getEmployeeId());
            stmt.setInt(4, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(IngredientReciept obj) {
        String sql = "DELETE FROM ingredient_receipt WHERE ingredient_receipt_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<IngredientReciept> getAll(String where, String order) {
        ArrayList<IngredientReciept> list = new ArrayList();
        String sql = "SELECT * FROM ingredient_receipt where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                IngredientReciept ingredient_reciept = IngredientReciept.fromRS(rs);
                list.add(ingredient_reciept);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
