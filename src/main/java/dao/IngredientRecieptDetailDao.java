/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.IngredientRecieptDetail;

/**
 *
 * @author Asus Vivobook
 */
public class IngredientRecieptDetailDao implements Dao<IngredientRecieptDetail>{

    @Override
    public IngredientRecieptDetail get(int id) {
        IngredientRecieptDetail ingredientRecieptDetail = null;
        String sql = "SELECT * FROM ingredient_reciept_detail WHERE ingredient_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ingredientRecieptDetail = IngredientRecieptDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return ingredientRecieptDetail;
    }

    @Override
    public List<IngredientRecieptDetail> getAll() {
        ArrayList<IngredientRecieptDetail> list = new ArrayList();
        String sql = "SELECT * FROM ingredient_reciept_detail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                IngredientRecieptDetail ingredientRecieptDetail = IngredientRecieptDetail.fromRS(rs);
                list.add(ingredientRecieptDetail);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<IngredientRecieptDetail> getAll(String order) {
        ArrayList<IngredientRecieptDetail> list = new ArrayList();
        String sql = "SELECT * FROM ingredient_reciept_detail ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                IngredientRecieptDetail ingredientRecieptDetail = IngredientRecieptDetail.fromRS(rs);
                list.add(ingredientRecieptDetail);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public IngredientRecieptDetail save(IngredientRecieptDetail obj) {
        String sql = "INSERT INTO ingredient_reciept_detail (ingredient_reciept_id, ingredient_id, ingredient_name, ingredient_price, ingredient_qty, ingredient_total)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getRecieptId());
            stmt.setInt(2, obj.getIngredientId());
            stmt.setString(3, obj.getName());
            stmt.setFloat(4, obj.getPrice());
            stmt.setInt(5, obj.getQty());
            stmt.setFloat(6, obj.getTotal());

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public IngredientRecieptDetail update(IngredientRecieptDetail obj) {
        String sql = "UPDATE ingredient_reciept_detail"
                + " SET ingredient_reciept_id = ?, ingredient_id = ?,ingredient_name = ?,ingredient_price = ?,ingredient_qty = ?,ingredient_total = ? "
                + " WHERE ingredient_reciept_detail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getRecieptId());
            stmt.setInt(2, obj.getIngredientId());
            stmt.setString(3, obj.getName());
            stmt.setFloat(4, obj.getPrice());
            stmt.setInt(5, obj.getQty());
            stmt.setFloat(6, obj.getTotal());
            stmt.setInt(7, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(IngredientRecieptDetail obj) {
        String sql = "DELETE FROM ingredient_reciept_detail WHERE ingredient_reciept_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<IngredientRecieptDetail> getAll(String where, String order) {
        ArrayList<IngredientRecieptDetail> list = new ArrayList();
        String sql = "SELECT * FROM ingredient_reciept_detail where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                IngredientRecieptDetail ingredientRecieptDetail = IngredientRecieptDetail.fromRS(rs);
                list.add(ingredientRecieptDetail);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
}
