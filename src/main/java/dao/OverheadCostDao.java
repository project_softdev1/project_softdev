/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.OverheadCost;

/**
 *
 * @author Gift
 */
public class OverheadCostDao {
    
    public OverheadCost save(OverheadCost obj) {
        String sql = "INSERT INTO overhead_cost (owner_id, overhead_cost_total)"
                + "VALUES(?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getOwnerId());
            stmt.setFloat(2, obj.getTotal());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }
    
    public OverheadCost update(OverheadCost obj) {
        String sql = "UPDATE overhead_cost"
                + " SET owner_id = ?, overhead_cost_total = ?"
                + " WHERE overhead_cost_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getOwnerId());
            stmt.setFloat(2, obj.getTotal());
            stmt.setInt(3, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
    
    public int delete(OverheadCost obj) {
        String sql = "DELETE FROM overhead_cost WHERE overhead_cost_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }
    
    public List<OverheadCost> getAll() {
        ArrayList<OverheadCost> list = new ArrayList();
        String sql = "SELECT * FROM overhead_cost";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                OverheadCost overhead_cost = OverheadCost.fromRS(rs);
                list.add(overhead_cost);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    //get all + where + order by
    public List<OverheadCost> getAll(String where, String order) {
        ArrayList<OverheadCost> list = new ArrayList();
        String sql = "SELECT * FROM overhead_cost where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OverheadCost overhead_cost = OverheadCost.fromRS(rs);
                list.add(overhead_cost);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    // get all + order by
    public List<OverheadCost> getAll(String order) {
        ArrayList<OverheadCost> list = new ArrayList();
        String sql = "SELECT * FROM overhead_cost  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OverheadCost overhead_cost = OverheadCost.fromRS(rs);
                list.add(overhead_cost);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public OverheadCost getById(int id) {
        OverheadCost overhead_cost = null;
        String sql = "SELECT * FROM overhead_cost WHERE overhead_cost_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                overhead_cost = OverheadCost.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return overhead_cost;
    }
    
    public OverheadCost getByOwnerId(int ownerId) {
        OverheadCost overhead_cost = null;
        String sql = "SELECT * FROM overhead_cost WHERE owner_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, ownerId);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                overhead_cost = OverheadCost.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return overhead_cost;
    }
    
    public OverheadCost getByTotal(float total) {
        OverheadCost overhead_cost = null;
        String sql = "SELECT * FROM overhead_cost WHERE owner_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, total);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                overhead_cost = OverheadCost.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return overhead_cost;
    }
}
