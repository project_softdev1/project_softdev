/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Owner;

/**
 *
 * @author Gift
 */
public class OwnerDao {
    
    public Owner save(Owner obj) {
        String sql = "INSERT INTO owner (owner_name, owner_tel, owner_password, owner_login)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getTel());
            stmt.setString(3, obj.getPassword());
            stmt.setString(4, obj.getLogin());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }
    
    public Owner update(Owner obj) {
        String sql = "UPDATE owner"
                + " SET owner_name = ?, owner_tel = ?, owner_password = ?, owner_login = ?"
                + " WHERE owner_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getTel());
            stmt.setString(3, obj.getPassword());
            stmt.setString(4, obj.getLogin());
            stmt.setInt(5, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
    
    public int delete(Owner obj) {
        String sql = "DELETE FROM owner WHERE owner_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }
    
    public List<Owner> getAll() {
        ArrayList<Owner> list = new ArrayList();
        String sql = "SELECT * FROM owner";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Owner owner = Owner.fromRS(rs);
                list.add(owner);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    //get all + where + order by
    public List<Owner> getAll(String where, String order) {
        ArrayList<Owner> list = new ArrayList();
        String sql = "SELECT * FROM owner where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Owner owner = Owner.fromRS(rs);
                list.add(owner);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    // get all + order by
    public List<Owner> getAll(String order) {
        ArrayList<Owner> list = new ArrayList();
        String sql = "SELECT * FROM owner  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Owner owner = Owner.fromRS(rs);
                list.add(owner);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public Owner getById(int id) {
        Owner owner = null;
        String sql = "SELECT * FROM owner WHERE owner_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                owner = Owner.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return owner;
    }
    
    public Owner getByName(String name) {
        Owner owner = null;
        String sql = "SELECT * FROM owner WHERE owner_name=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                owner = Owner.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return owner;
    }
    
    public Owner getByTel(String tel) {
        Owner owner = null;
        String sql = "SELECT * FROM owner WHERE owner_tel=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, tel);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                owner = Owner.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return owner;
    }
    
    public Owner getByPassword(String password) {
        Owner owner = null;
        String sql = "SELECT * FROM owner WHERE owner_password=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, password);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                owner = Owner.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return owner;
    }
    
    public Owner getByLogin(String login) {
        Owner owner = null;
        String sql = "SELECT * FROM owner WHERE owner_login=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, login);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                owner = Owner.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return owner;
    }
}
