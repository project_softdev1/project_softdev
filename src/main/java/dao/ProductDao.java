/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Product;

/**
 *
 * @author Gift
 */
public class ProductDao {
    
    public Product save(Product obj) {
        String sql = "INSERT INTO product (product_name, product_price, product_type, category_id, product_sweet_level)"
                + "VALUES(?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setFloat(2, obj.getPrice());
            stmt.setString(3, obj.getType());
            stmt.setInt(4, obj.getCategoryId());
            stmt.setString(5, obj.getSweetLevel());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }
    
    public Product update(Product obj) {
        String sql = "UPDATE product"
                + " SET product_name = ?, product_price = ?, product_type = ?, category_id = ?, product_sweet_level = ?"
                + " WHERE product_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
           stmt.setString(1, obj.getName());
            stmt.setFloat(2, obj.getPrice());
            stmt.setString(3, obj.getType());
            stmt.setInt(4, obj.getCategoryId());
            stmt.setString(5, obj.getSweetLevel());
            stmt.setInt(6, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
    
    public int delete(Product obj) {
        String sql = "DELETE FROM product WHERE product_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }
    
    public List<Product> getAll() {
        ArrayList<Product> list = new ArrayList();
        String sql = "SELECT * FROM product";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Product product = Product.fromRS(rs);
                list.add(product);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    //get all + where + order by
    public List<Product> getAll(String where, String order) {
        ArrayList<Product> list = new ArrayList();
        String sql = "SELECT * FROM product where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Product product = Product.fromRS(rs);
                list.add(product);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    // get all + order by
    public List<Product> getAll(String order) {
        ArrayList<Product> list = new ArrayList();
        String sql = "SELECT * FROM product  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Product product = Product.fromRS(rs);
                list.add(product);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public Product getById(int id) {
        Product product = null;
        String sql = "SELECT * FROM product WHERE product_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                product = Product.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return product;
    }
    
    public Product getByName(String name) {
        Product product = null;
        String sql = "SELECT * FROM product WHERE product_name=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                product = Product.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return product;
    }
    
    public Product getByTel(String tel) {
        Product product = null;
        String sql = "SELECT * FROM product WHERE product_tel=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, tel);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                product = Product.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return product;
    }
    
    public Product getByRole(int role) {
        Product product = null;
        String sql = "SELECT * FROM product WHERE product_role=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, role);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                product = Product.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return product;
    }
    
    public Product getByLogin(String login) {
        Product product = null;
        String sql = "SELECT * FROM product WHERE product_login=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, login);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                product = Product.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return product;
    }
     public List<Product> getListById(int id) {
        ArrayList<Product> list = new ArrayList();
        String sql = "SELECT * FROM product WHERE product_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Product product = Product.fromRS(rs);
                list.add(product);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
     
    
}
