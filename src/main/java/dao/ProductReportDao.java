/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.ProductReport;
import model.ReceiptDetail;

/**
 *
 * @author acer
 */
public class ProductReportDao {
    public List<ProductReport> getTopSellers(){
    ArrayList<ProductReport> list = new ArrayList();
        String sql = """
                     WITH RankedProducts AS (
                         SELECT product_id,
                                product_name,
                                SUM(product_qty) AS QTY,
                                ROW_NUMBER() OVER (ORDER BY SUM(product_qty) DESC) AS Rank
                           FROM receipt_detail
                          GROUP BY product_id,
                                   product_name
                     )
                     SELECT Rank,
                            product_name
                       FROM RankedProducts
                      LIMIT 10""";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ProductReport obj = ProductReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
