/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Promotion;

//import model.PromotionDetail;
/**
 *
 * @author Diarydear
 */
public class PromotionDao {

    public Promotion save(Promotion obj) {
        String sql = "INSERT INTO promotion (promotion_name,promotion_description, promotion_startdate, promotion_enddate, promotion_discount, product_category, unit)"
                + "VALUES(?, ?, ?, ?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getDescription());
            stmt.setString(3, obj.getStartDate());
            stmt.setString(4, obj.getEndDate());
            stmt.setFloat(5, obj.getPromotionDiscount());
            stmt.setString(6, obj.getProductCategory());
            stmt.setInt(7, obj.getUnit());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    public Promotion update(Promotion obj) {
        String sql = "UPDATE promotion"
                + " SET promotion_name = ?, promotion_description = ?"
                + ", promotion_startdate = ?, promotion_enddate = ? ,promotion_discount = ?,product_category= ?,unit= ? "
                + " WHERE promotion_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getDescription());
            stmt.setString(3, obj.getStartDate());
            stmt.setString(4, obj.getEndDate());
            stmt.setFloat(5, obj.getPromotionDiscount());
            stmt.setString(6, obj.getProductCategory());
            stmt.setInt(7, obj.getUnit());
            stmt.setInt(8, obj.getId());

//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public int delete(Promotion obj) {
        String sql = "DELETE FROM promotion WHERE promotion_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public List<Promotion> getAll() {
        ArrayList<Promotion> list = new ArrayList();
        String sql = "SELECT * FROM promotion";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Promotion promotion = Promotion.fromRS(rs);
                list.add(promotion);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    //get all + where + order by
    public List<Promotion> getAll(String where, String order) {
        ArrayList<Promotion> list = new ArrayList();
        String sql = "SELECT * FROM promotion where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Promotion promotion = Promotion.fromRS(rs);
                list.add(promotion);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    // get all + order by
    public List<Promotion> getAll(String order) {
        ArrayList<Promotion> list = new ArrayList();
        String sql = "SELECT * FROM promotion  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Promotion promotion = Promotion.fromRS(rs);
                list.add(promotion);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public Promotion getById(int id) {
        Promotion promotion = null;
        String sql = "SELECT * FROM promotion WHERE promotion_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                promotion = Promotion.fromRS(rs);
//                PromotionDetailDao pdd = new PromotionDetailDao();
//                ArrayList<PromotionDetail> promotionDetails = (ArrayList<PromotionDetail>) pdd.getAll("promotion_id ="+promotion.getId()," promotion_detail_id");
//                promotion.setPromotionDetail(promotionDetails);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return promotion;
    }

    public Promotion getCategoryById(int id) {
        Promotion promotion = null;
        String sql = "SELECT product_category FROM promotion WHERE promotion_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                promotion = Promotion.fromRS(rs);
//                PromotionDetailDao pdd = new PromotionDetailDao();
//                ArrayList<PromotionDetail> promotionDetails = (ArrayList<PromotionDetail>) pdd.getAll("promotion_id ="+promotion.getId()," promotion_detail_id");
//                promotion.setPromotionDetail(promotionDetails);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return promotion;
    }

    public List<String> getCategory() {
        ArrayList<String> list = new ArrayList();
        String sql = "SELECT product_category FROM promotion";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                String productCategoryText = rs.getString("product_category");
                list.add(productCategoryText);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Promotion> getProductCategory() {
        ArrayList<Promotion> list = new ArrayList();
        String sql = "SELECT product_category FROM promotion";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Promotion promotion = Promotion.fromRS(rs);
                list.add(promotion);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
