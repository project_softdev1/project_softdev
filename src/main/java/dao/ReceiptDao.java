/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Receipt;
import model.ReceiptDetail;

/**
 *
 * @author Gift
 */
public class ReceiptDao {
    
    public Receipt save(Receipt obj) {
        String sql = "INSERT INTO receipt (receipt_total, receipt_cash, receipt_qty, employee_id, customer_id,promotion_id,total_discount,owner_id)"
                + "VALUES(?, ?, ?, ?, ?, ?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotal());
            stmt.setFloat(2, obj.getCash());
            stmt.setInt(3, obj.getQty());
            stmt.setInt(4, obj.getEmployeeId());
            stmt.setInt(5, obj.getCustomerId());
            stmt.setInt(6, obj.getPromotionId());
            stmt.setFloat(7, obj.getTotalDiscount());
            stmt.setInt(8, obj.getOwnerid());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }
    
    public Receipt update(Receipt obj) {
        String sql = "UPDATE receipt"
                + " SET receipt_total = ?, receipt_cash = ?, receipt_qty = ?, employee_id = ?, customer_id = ?,promotion_id = ?, total_discount = ?,owner_id=?"
                + " WHERE receipt_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
           stmt.setFloat(1, obj.getTotal());
                      stmt.setFloat(1, obj.getTotal());
            stmt.setFloat(2, obj.getCash());
            stmt.setInt(3, obj.getQty());
            stmt.setInt(4, obj.getEmployeeId());
            stmt.setInt(5, obj.getCustomerId());
            stmt.setInt(6, obj.getPromotionId());
            stmt.setFloat(7, obj.getTotalDiscount());
            stmt.setInt(8, obj.getOwnerid());
            stmt.setInt(9, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
    
    public int delete(Receipt obj) {
        String sql = "DELETE FROM receipt WHERE receipt_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }
    
    public List<Receipt> getAll() {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM receipt";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Receipt receipt = Receipt.fromRS(rs);
                list.add(receipt);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    //get all + where + order by
    public List<Receipt> getAll(String where, String order) {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM receipt where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt receipt = Receipt.fromRS(rs);
                list.add(receipt);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    // get all + order by
    public List<Receipt> getAll(String order) {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM receipt  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt receipt = Receipt.fromRS(rs);
                list.add(receipt);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public Receipt getById(int id) {
        Receipt receipt= null;
        String sql = "SELECT * FROM receipt WHERE receipt_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                receipt = Receipt.fromRS(rs);
                ReceiptDetailDao rdd = new ReceiptDetailDao();
                ArrayList<ReceiptDetail> receiptDetails = (ArrayList<ReceiptDetail>) rdd.getAll("receipt_id="+receipt.getId()," receipt_detail_id");
                receipt.setReceiptDetails(receiptDetails);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return receipt;
    }
    
   
 
    
}
