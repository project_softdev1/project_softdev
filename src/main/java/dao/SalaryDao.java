/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Salary;

/**
 *
 * @author Gift
 */
public class SalaryDao implements Dao<Salary> {
    
     @Override
    public Salary get(int id) {
        Salary salary = null;
        String sql = "SELECT * FROM salary WHERE salary_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                salary = Salary.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return salary;
    }

    public Salary save(Salary obj) {
        String sql = "INSERT INTO salary (employee_id, owner_id, salary_payment,salary_date)"
                + "VALUES(?, ?, ?,CURRENT_TIMESTAMP)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployeeId());
            stmt.setInt(2, obj.getOwnerId());
            stmt.setFloat(3, obj.getPayment());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    public Salary update(Salary obj) {
        String sql = "UPDATE salary"
                + " SET employee_id = ?, owner_id = ?, salary_payment = ?,salary_date=CURRENT_TIMESTAMP"
                + " WHERE salary_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployeeId());
            stmt.setInt(2, obj.getOwnerId());
            stmt.setFloat(3, obj.getPayment());
            stmt.setInt(4, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public int delete(Salary obj) {
        String sql = "DELETE FROM salary WHERE salary_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public List<Salary> getAll() {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM salary";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Salary salary = Salary.fromRS(rs);
                list.add(salary);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    //get all + where + order by
    public List<Salary> getAll(String where, String order) {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM salary where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary salary = Salary.fromRS(rs);
                list.add(salary);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    // get all + order by
    public List<Salary> getAll(String order) {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM salary  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary salary = Salary.fromRS(rs);
                list.add(salary);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public Salary getById(int id) {
        Salary salary = null;
        String sql = "SELECT * FROM salary WHERE salary_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                salary = Salary.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return salary;
    }
    
    public List<Salary> getSalaryByPayment(){
    ArrayList<Salary> list = new ArrayList();
        String sql = """
                     SELECT salary_id , salary_date , employee_id,owner_id,employee_name,salary_payment
                     FROM salary NATURAL JOIN employee""";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Salary obj = Salary.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }


}
