/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;


import dao.EmployeeDao;
import dao.SalaryDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HP
 */
public class CheckInOut {
    private int id;
    private Date CheckInDate;
    private String CheckOutDate;
    private int employeeId;
    private int salaryId;
    
    private Employee employee;
    private Salary salary;

    public CheckInOut(int id, Date CheckInDate, String CheckOutDate, int employeeId, int salaryId) {
        this.id = id;
        this.CheckInDate = CheckInDate;
        this.CheckOutDate = CheckOutDate;
        this.employeeId = employeeId;
        this.salaryId = salaryId;
    }
    
    /**
     *
     * @param CheckInDate
     * @param CheckOutDate
     * @param employeeId
     * @param salaryId
     */
    public CheckInOut(Date CheckInDate, String CheckOutDate, int employeeId, int salaryId) {
        this.id = -1;
        this.CheckInDate = CheckInDate;
        this.CheckOutDate = CheckOutDate;
        this.employeeId = employeeId;
        this.salaryId = salaryId;
    }
     
    public CheckInOut() {
        this.id = -1;
        this.CheckInDate = null;
        this.CheckOutDate = "-";
        this.employeeId = 0;
        this.salaryId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Salary getSalary() {
        return salary;
    }

    public void setSalary(Salary salary) {
        this.salary = salary;
    }

    public Date getCheckInDate() {
        return CheckInDate;
    }

    public void setCheckInDate(Date CheckInDate) {
        this.CheckInDate = CheckInDate;
    }

    public String getCheckOutDate() {
        return CheckOutDate;
    }

    public void setCheckOutDate(String CheckOutDate) {
            this.CheckOutDate = CheckOutDate;
         }
    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public int getSalaryId() {
        return salaryId;
    }

    public void setSalaryId(int salaryId) {
        this.salaryId = salaryId;
    }

    @Override
    public String toString() {
        return "CheckInOut{" + "id=" + id + ", CheckInDate=" + CheckInDate + ", CheckOutDate=" + CheckOutDate + ", employeeId=" + employeeId + ", salaryId=" + salaryId + ", employee=" + employee + ", salary=" + salary + '}';
    }



    public static CheckInOut fromRS(ResultSet rs) {
        CheckInOut checkInOut = new CheckInOut();
        try {
            checkInOut.setId(rs.getInt("checkinout_id"));
            checkInOut.setCheckInDate(rs.getTimestamp("checkinout_indatetime"));
            checkInOut.setCheckOutDate(rs.getString("checkinout_outdatetime"));
            checkInOut.setEmployeeId(rs.getInt("employee_id"));
            checkInOut.setSalaryId(rs.getInt("salary_id"));
            
            //population
            SalaryDao salaryDao = new SalaryDao();
            EmployeeDao employeeDao = new EmployeeDao();
            Employee employee =  employeeDao.get(checkInOut.getEmployeeId());
            Salary salary = salaryDao.get(checkInOut.getSalaryId());
            checkInOut.setEmployee(employee);
            checkInOut.setSalary(salary);
            
        } catch (SQLException ex) {
            Logger.getLogger(CheckInOut.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkInOut;
    } 
    
}
