/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gift
 */
public class Employee {
    
    private int id;
    private String name;
    private String tel;
    private int role; //int like ajarn? or ja aow pen String role leoy?
    private String login;
    private String password;

    public Employee(int id, String name, String tel, int role, String login, String password) {
        this.id = id;
        this.name = name;
        this.tel = tel;
        this.role = role;
        this.login = login;
        this.password = password;
    }
    
    public Employee(String name, String tel, int role, String login, String password) {
        this.id = -1;
        this.name = name;
        this.tel = tel;
        this.role = role;
        this.login = login;
        this.password = password;
    }
    
    public Employee() {
        this.id = -1;
        this.name = "";
        this.tel = "";
        this.role = 0;
        this.login = "";
        this.password = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", tel=" + tel + ", role=" + role + ", login=" + login + ", password=" + password + '}';
    }
    
    public static Employee fromRS(ResultSet rs) {
        Employee employee = new Employee();
        try {
            employee.setId(rs.getInt("employee_id"));
            employee.setName(rs.getString("employee_name"));
            employee.setTel(rs.getString("employee_tel"));
            employee.setRole(rs.getInt("employee_role"));
            employee.setLogin(rs.getString("employee_login"));
            employee.setPassword(rs.getString("employee_password"));
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return employee;
    }
    
}
