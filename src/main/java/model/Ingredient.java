/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author user
 */
public class Ingredient {
    
    private int id;
    private String name;
    private int qty;
    private float price;
    
    /**
     *
     * @param id
     * @param name
     * @param qty
     * @param price
     */
    public Ingredient(int id, String name, int qty, float price) {
        this.id = id;
        this.name = name;
        this.qty = qty;
        this.price = price;
    }
    public Ingredient(String name, int qty, float price) {
        this.id = -1;
        this.name = name;
        this.qty = qty;
        this.price = price;
    }
    
    public Ingredient(){
        this.id = -1 ;
        this.name = "" ;
        this.qty = 0 ;
        this.price = 0 ;
    }
        
        public int getId(){
            return id;
        }
        
        public void setId(int id){
            this.id = id;
        }
        
        public String getName(){
            return name;
        }
        
        public void setName(String name){
            this.name = name;
        }
        
        public int getQty(){
            return qty;
        }
        
        public void setQty(int qty){
            this.qty = qty;
        }
        
        public float getPrice(){
            return price;
        }
        
        public void setPrice(float price){
            this.price = price;
        }
        
       @Override
       public String toString(){
           return "Ingredient{" + "id=" + id + ", name=" + name + ", qty=" + qty + ", price=" + price + '}';
       }
       
       public static Ingredient fromRS(ResultSet rs){
           Ingredient ingredient = new Ingredient();
           try {
               ingredient.setId(rs.getInt("ingredient_id"));
               ingredient.setName(rs.getString("ingredient_name"));
               ingredient.setQty(rs.getInt("ingredient_qty"));
               ingredient.setPrice(rs.getFloat("ingredient_price"));
               
           } catch (SQLException ex){
               Logger.getLogger(Ingredient.class.getName()).log(Level.SEVERE, null, ex);
               return null;
           }
           return ingredient;
       }
        
}

