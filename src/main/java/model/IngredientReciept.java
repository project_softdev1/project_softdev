/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus Vivobook
 */
public class IngredientReciept {
    private int id;
    private Date datetime;
    private float total;
    private String vendor;
    private int employeeId;
    private Employee employee;

    public IngredientReciept(int id, Date datetime, float total, String vendor, int employeeId) {
        this.id = id;
        this.datetime = datetime;
        this.total = total;
        this.vendor = vendor;
        this.employeeId = employeeId;
    }
    
    public IngredientReciept(Date datetime, float total, String vendor, int employeeId) {
        this.id = -1;
        this.datetime = datetime;
        this.total = total;
        this.vendor = vendor;
        this.employeeId = employeeId;
    }
    
    public IngredientReciept() {
        this.id = -1;
        this.datetime = null;
        this.total = 0;
        this.vendor = "";
        this.employeeId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    
    

    @Override
    public String toString() {
        return "IngredientReciept{" + "id=" + id + ", datetime=" + datetime + ", total=" + total + ", vendor=" + vendor + ", employeeId=" + employeeId + '}';
    }
    
    public static IngredientReciept fromRS(ResultSet rs) {
        IngredientReciept ingredientReciept = new IngredientReciept();
        try {
            ingredientReciept.setId(rs.getInt("ingredient_receipt_id"));
            ingredientReciept.setDatetime(rs.getTimestamp("ingredient_receipt_datetime"));
            ingredientReciept.setTotal(rs.getFloat("ingredient_receipt_total"));
            ingredientReciept.setVendor(rs.getString("ingredient_receipt_vendor"));
            ingredientReciept.setEmployeeId(rs.getInt("employee_id"));
            
            //population
            EmployeeDao employeeDao = new EmployeeDao();
            Employee employee =  employeeDao.get(ingredientReciept.getEmployeeId());
            ingredientReciept.setEmployee(employee);
            
            
        } catch (SQLException ex) {
            Logger.getLogger(IngredientReciept.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return ingredientReciept;
    } 
}
