/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dao.IngredientDao;
import dao.IngredientRecieptDao;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Asus Vivobook
 */
public class IngredientRecieptDetail {
    private int id;
    private int recieptId;
    private int ingredientId;
    private String name;
    private float price;
    private int qty;
    private float total;
    private IngredientReciept ingredientReciept;
    private Ingredient ingredient;

    public IngredientRecieptDetail(int id, int recieptId, int ingredientId, String name, float price, int qty, float total) {
        this.id = id;
        this.recieptId = recieptId;
        this.ingredientId = ingredientId;
        this.name = name;
        this.price = price;
        this.qty = qty;
        this.total = total;
    }
    
    public IngredientRecieptDetail(int recieptId, int ingredientId, String name, float price, int qty, float total) {
        this.id = -1;
        this.recieptId = recieptId;
        this.ingredientId = ingredientId;
        this.name = name;
        this.price = price;
        this.qty = qty;
        this.total = total;
    }
    
    public IngredientRecieptDetail() {
        this.id = -1;
        this.recieptId = 0;
        this.ingredientId = 0;
        this.name = "";
        this.price = 0;
        this.qty = 0;
        this.total = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRecieptId() {
        return recieptId;
    }

    public void setRecieptId(int recieptId) {
        this.recieptId = recieptId;
    }

    public int getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(int ingredientId) {
        this.ingredientId = ingredientId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public IngredientReciept getIngredientReciept() {
        return ingredientReciept;
    }

    public void setIngredientReciept(IngredientReciept ingredientReciept) {
        this.ingredientReciept = ingredientReciept;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    @Override
    public String toString() {
        return "IngredientRecieptDetail{" + "id=" + id + ", recieptId=" + recieptId + ", ingredientId=" + ingredientId + ", name=" + name + ", price=" + price + ", qty=" + qty + ", total=" + total + ", ingredientReciept=" + ingredientReciept + ", ingredient=" + ingredient + '}';
    }

    
    
    public static IngredientRecieptDetail fromRS(ResultSet rs) {
        IngredientRecieptDetail ird = new IngredientRecieptDetail();
        try {
            ird.setId(rs.getInt("ingredient_receipt_detail_id"));
            ird.setRecieptId(rs.getInt("ingredient_receipt_id"));
            ird.setIngredientId(rs.getInt("ingredient_id"));
            ird.setName(rs.getString("ingredient_name"));
            ird.setPrice(rs.getFloat("ingredient_price"));
            ird.setQty(rs.getInt("ingredient_qty"));
            ird.setTotal(rs.getFloat("ingredient_total"));
            
            //population
            IngredientDao ingredientDao = new IngredientDao();
            Ingredient ingredient = ingredientDao.get(ird.getIngredientId());
            ird.setIngredient(ingredient);
            
            IngredientRecieptDao irDao = new IngredientRecieptDao();
            IngredientReciept ir = irDao.get(ird.getRecieptId());
            ird.setIngredientReciept(ir);
  
        } catch (SQLException ex) {
            Logger.getLogger(IngredientRecieptDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return ird;
    }
}
