/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HP
 */
public class OverheadCost {

    private int id;
    private int ownerId;
    private Date datetime;
    private float total;

    public OverheadCost(int id, int ownerId, Date datetime, float total) {
        this.id = id;
        this.ownerId = ownerId;
        this.datetime = datetime;
        this.total = total;
    }

    public OverheadCost(int ownerId, Date datetime, float total) {
        this.id = -1;
        this.ownerId = ownerId;
        this.datetime = datetime;
        this.total = total;
    }

    public OverheadCost() {
        this.id = -1;
        this.ownerId = 0;
        this.datetime = null;
        this.total = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "OverheadCost{" + "id=" + id + ", ownerId=" + ownerId + ", datetime=" + datetime + ", total=" + total + '}';
    }

    public static OverheadCost fromRS(ResultSet rs) {
        OverheadCost overheadCost = new OverheadCost();
        try {
            overheadCost.setId(rs.getInt("overhead_cost_id"));
            overheadCost.setOwnerId(rs.getInt("owner_id"));
            overheadCost.setDatetime(rs.getTimestamp("overhead_cost_datetime"));
            overheadCost.setTotal(rs.getFloat("overhead_cost_total"));
        } catch (SQLException ex) {
            Logger.getLogger(OverheadCost.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return overheadCost;
    }

}
