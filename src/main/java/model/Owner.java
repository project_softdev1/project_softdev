/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gift
 */
public class Owner {
    
    private int id;
    private String name;
    private String tel;
    private String password;
    private String login;

    public Owner(int id, String name, String tel, String password, String login) {
        this.id = id;
        this.name = name;
        this.tel = tel;
        this.password = password;
        this.login = login;
    }
    
    public Owner(String name, String tel, String password, String login) {
        this.id = -1;
        this.name = name;
        this.tel = tel;
        this.password = password;
        this.login = login;
    }
    
    public Owner() {
        this.id = -1;
        this.name = "";
        this.tel = "";
        this.password = "";
        this.login = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String toString() {
        return "Owner{" + "id=" + id + ", name=" + name + ", tel=" + tel + ", password=" + password + ", login=" + login + '}';
    }

    
    
    public static Owner fromRS(ResultSet rs) {
        Owner owner = new Owner();
        try {
            owner.setId(rs.getInt("owner_id"));
            owner.setName(rs.getString("owner_name"));
            owner.setTel(rs.getString("owner_tel"));
            owner.setPassword(rs.getString("owner_password"));
            owner.setLogin(rs.getString("owner_login"));
        } catch (SQLException ex) {
            Logger.getLogger(Owner.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return owner;
    }
}
