/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class ProductReport {
    private int rank;
    private String name;

    public ProductReport(int rank, String name) {
        this.rank = rank;
        this.name = name;
    }
    
    public ProductReport(String name) {
        this.rank = 0;
        this.name = name;
    }
    
    public ProductReport() {
        this(0,"");
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ProductReport{" + "rank=" + rank + ", name=" + name + '}';
    }
    
    public static ProductReport fromRS(ResultSet rs) {
       ProductReport pr = new ProductReport();
        try {
            pr.setRank(rs.getInt("Rank"));
            pr.setName(rs.getString("product_name"));
           
        } catch (SQLException ex) {
            Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return pr;
    }
    
    
}
