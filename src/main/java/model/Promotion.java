/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Diarydear
 */
public class Promotion {

    private int id;
    private String name;
    private String description;
    private String startDate;
    private String endDate;
    private float promotionDiscount;
    private String productCategory;
    private int unit;
//    private ArrayList<PromotionDetail> promotionDetail = new ArrayList<PromotionDetail>();

    public Promotion(int id, String name, String description, String startDate, String endDate, float promotionDiscount, String productCategory, int unit) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.promotionDiscount = promotionDiscount;
        this.productCategory = productCategory;
        this.unit = unit;
    }

    public Promotion(String name, String description, String startDate, String endDate, float promotionDiscount, String productCategory, int unit) {
        this.id = -1;
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.promotionDiscount = promotionDiscount;
        this.productCategory = productCategory;
        this.unit = unit;
    }

    public Promotion() {
        this.id = -1;
        this.name = "";
        this.description = "";
        this.startDate = "";
        this.endDate = "";
        this.promotionDiscount = 0;
        this.productCategory = "";
        this.unit = 0;
    }

    public float getPromotionDiscount() {
        return promotionDiscount;
    }

    public void setPromotionDiscount(float promotionDiscount) {
        this.promotionDiscount = promotionDiscount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

//    public ArrayList<PromotionDetail> getPromotionDetail() {
//        return promotionDetail;
//    }
//
//    public void setPromotionDetail(ArrayList<PromotionDetail> promotionDetail) {
//        this.promotionDetail = promotionDetail;
//    }
    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "Promotion{" + "id=" + id + ", name=" + name + ", description=" + description + ", startDate=" + startDate + ", endDate=" + endDate + ", promotionDiscount=" + promotionDiscount + ", productCategory=" + productCategory + ", unit=" + unit + '}';
    }

//     public void addPromotionDetail(PromotionDetail promotionDetails){
//          promotionDetail.add(promotionDetails);
//    }
//     
//    public void addPromotionDetail(Product product,float discount){
//          PromotionDetail promotionDetails = new PromotionDetail(-1,discount, product.getId());
//          promotionDetail.add(promotionDetails);
//    }
//    
    public static Promotion fromRS(ResultSet rs) {
        Promotion promotion = new Promotion();
        try {
            promotion.setId(rs.getInt("promotion_id"));
            promotion.setName(rs.getString("promotion_name"));
            promotion.setDescription(rs.getString("promotion_description"));
            promotion.setStartDate(rs.getString("promotion_startdate"));
            promotion.setEndDate(rs.getString("promotion_enddate"));
            promotion.setPromotionDiscount(rs.getFloat("promotion_discount"));
            promotion.setProductCategory(rs.getString("product_category"));
            promotion.setUnit(rs.getInt("unit"));

        } catch (SQLException ex) {
            Logger.getLogger(Promotion.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return promotion;
    }
}
