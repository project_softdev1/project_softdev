/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dao.CustomerDao;
import dao.EmployeeDao;
import dao.OwnerDao;
import dao.PromotionDao;
import dao.ReceiptDetailDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import service.PromotionService;

/**
 *
 * @author Lenovo
 */
public class Receipt {

    private int id;
    private Date datetime;
    private float total;
    private float cash;
    private int qty;
    private int employeeId;
    private int customerId;
    private int promotionId;
    private float totalDiscount;
    private Employee employee;
    private Customer customer;
    private Promotion promotion;
    private ArrayList<Promotion> promotions = new ArrayList<Promotion>();
    private ArrayList<ReceiptDetail> receiptDetails = new ArrayList<ReceiptDetail>();
    private int ownerid;
    private Owner owner;

    public Receipt(int id, Date datetime, float total, float cash, int qty, int employeeId, int customerId, int promotionId, float totalDiscount, int ownerid) {
        this.id = id;
        this.datetime = datetime;
        this.total = total;
        this.cash = cash;
        this.qty = qty;
        this.employeeId = employeeId;
        this.customerId = customerId;
        this.promotionId = promotionId;
        this.totalDiscount = totalDiscount;
        this.ownerid = ownerid;
    }

    public Receipt(Date datetime, float total, float cash, int qty, int employeeId, int customerId, int promotionId, float totalDiscount, int ownerid) {
        this.id = -1;
        this.datetime = datetime;
        this.total = total;
        this.cash = cash;
        this.qty = qty;
        this.employeeId = employeeId;
        this.customerId = customerId;
        this.promotionId = promotionId;
        this.totalDiscount = totalDiscount;
        this.ownerid = ownerid;
    }

    

    public Receipt(float total, float cash, int qty, int employeeId, int customerId, int promotionId, float totalDiscount, int ownerid) {
        this.id = -1;
        this.datetime = null;
        this.total = total;
        this.cash = cash;
        this.qty = qty;
        this.employeeId = employeeId;
        this.customerId = customerId;
        this.promotionId = promotionId;
        this.totalDiscount = totalDiscount;
        this.ownerid = customerId;

    }

    public Receipt(float cash, int employeeId, int customerId, float totalDiscount) {
        this.id = -1;
        this.datetime = null;
        this.total = 0;
        this.cash = cash;
        this.qty = 0;
        this.employeeId = employeeId;
        this.customerId = customerId;
        this.totalDiscount = totalDiscount;
this.ownerid = customerId;
    }

    public Receipt() {
        this.id = -1;
        this.datetime = null;
        this.total = 0;
        this.cash = 0;
        this.qty = 0;
        this.employeeId = 0;
        this.customerId = 0;
        this.promotionId = 0;
        this.totalDiscount = 0;
        this.ownerid = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getCash() {
        return cash;
    }

    public void setCash(float cash) {
        this.cash = cash;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public float getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(float totalDiscount) {
        this.totalDiscount = totalDiscount;

    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
        this.employeeId = employee.getId();

    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
        this.customerId = customer.getId();

    }

    public ArrayList<ReceiptDetail> getReceiptDetails() {
        return receiptDetails;
    }

    public void setReceiptDetails(ArrayList<ReceiptDetail> receiptDetails) {
        this.receiptDetails = receiptDetails;
    }

    public void addReceiptDetail(ReceiptDetail recieptDetail) {
        receiptDetails.add(recieptDetail);
        calculateTotal();
    }

    public int getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(int promotionId) {
        this.promotionId = promotionId;
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }

    public int getOwnerid() {
        return ownerid;
    }

    public void setOwnerid(int ownerid) {
        this.ownerid = ownerid;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }
    

    public void addReceiptDetail(Product product, int qty) {
//        Receipt r = new Receipt();
//        float rdis = r.getTotalDiscount();
       if(check(product)!=-1){
          int p = check(product);
          ReceiptDetailDao rdd = new ReceiptDetailDao();
          ReceiptDetail rd = receiptDetails.get(p);
          int dupQty = rd.getProductQty();
          rd.setProductQty(dupQty+qty);
       }
       
       else{
        ReceiptDetail rd = new ReceiptDetail(product.getId(), product.getName(),
                product.getPrice(), qty, qty * product.getPrice(), -1); 
        receiptDetails.add(rd);
     
       }calculateTotal();
//        calculateDiscount();
    }

    public void delRecieptDetail(ReceiptDetail recieptDetail) {
        receiptDetails.remove(recieptDetail);
        calculateTotal();
    }

    public static Receipt fromRS(ResultSet rs) {
        Receipt receipt = new Receipt();
        try {
            receipt.setId(rs.getInt("receipt_id"));
            receipt.setDatetime(rs.getTimestamp("receipt_datetime"));
            receipt.setTotal(rs.getFloat("receipt_total"));
            receipt.setCash(rs.getFloat("receipt_cash"));
            receipt.setQty(rs.getInt("receipt_qty"));
            receipt.setEmployeeId(rs.getInt("employee_id"));
            receipt.setCustomerId(rs.getInt("customer_id"));
            receipt.setPromotionId(rs.getInt("promotion_id"));
            receipt.setTotalDiscount(rs.getFloat("total_discount"));
            receipt.setOwnerid(rs.getInt("owner_id"));

            //Population
            CustomerDao customerDao = new CustomerDao();
            EmployeeDao employeeDao = new EmployeeDao();
            PromotionDao promotionDao = new PromotionDao();
            OwnerDao ownerDao = new OwnerDao();
            Customer customer = customerDao.getById(receipt.getCustomerId());
            Employee employee = employeeDao.getById(receipt.getEmployeeId());
            Promotion promotion = promotionDao.getById(receipt.getPromotionId());
            Owner owner = ownerDao.getById(receipt.getOwnerid());
            receipt.setCustomer(customer);
            receipt.setEmployee(employee);
            receipt.setPromotion(promotion);
            receipt.setOwner(owner);

        } catch (SQLException ex) {
            Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return receipt;
    }

    @Override
    public String toString() {
        return "Receipt{" + "id=" + id + ", datetime=" + datetime + ", total=" + total + ", cash=" + cash + ", qty=" + qty + ", employeeId=" + employeeId + ", customerId=" + customerId + ", promotionId=" + promotionId + ", totalDiscount=" + totalDiscount + ", employee=" + employee + ", customer=" + customer + ", promotion=" + promotion + ", promotions=" + promotions + ", receiptDetails=" + receiptDetails + ", ownerid=" + ownerid + ", owner=" + owner + '}';
    }

    

    public int check(Product product) {
        for (int i = 0; i < receiptDetails.size(); i++) {
            ReceiptDetail rd = receiptDetails.get(i);
            if (rd.getProductId() == product.getId()) {
                return i;
            }

        }
        return -1;
    }
    
//  public void calculateDiscount(){
//      int totalDiscount = 0;
//      for(Promotion p: promotions){
//          totalDiscount +=p.getPromotionDiscount();
//      }
//      this.totalDiscount = totalDiscount;
//  }
    public void calculateTotal() {
        int totalQty = 0;
        float totalP = 0.0f;
        for (ReceiptDetail rd : receiptDetails) {
            totalP += rd.getProductTotal();
            totalQty += rd.getProductQty();
        }
        this.qty = totalQty;
        this.total = totalP;
    }

}
