/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lenovo
 */
public class ReceiptDetail {

    private int id;
    private int productId;
    private String productName;
    private float productPrice;
    private int productQty;
    private float productTotal;
    private int receiptId;
  
//    private ArrayList<PromotionDetail> promotionDetail = new ArrayList<PromotionDetail>();

    public ReceiptDetail(int id, int productId, String productName, float productPrice, int productQty, float productTotal, int receiptId) {
        this.id = id;
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.productQty = productQty;
        this.productTotal = productTotal;
        this.receiptId = receiptId;
 
    }
     public ReceiptDetail(int productId, String productName, float productPrice, int productQty, float productTotal, int receiptId) {
        this.id = -1;
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.productQty = productQty;
        this.productTotal = productTotal;
        this.receiptId = receiptId;
       
    }
     public ReceiptDetail() {
        this.id = -1;
        this.productId = 0;
        this.productName = "";
        this.productPrice = 0;
        this.productQty = 0;
        this.productTotal = 0;
        this.receiptId = 0;
   
    }
   

   

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public float getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(float productPrice) {
        this.productPrice = productPrice;
    }

    public int getProductQty() {
        return productQty;
    }

    public void setProductQty(int productQty) {
        this.productQty = productQty;
        productTotal = productQty*productPrice;
    }
  
    public float getProductTotal() {
        return productTotal;
    }

    public void setProductTotal(float productTotal) {
        this.productTotal = productTotal;
    }

    public int getReceiptId() {
        return receiptId;
    }

    public void setReceiptId(int receiptId) {
        this.receiptId = receiptId;
    }

//    public int getPromotionDetailId() {
//        return promotionDetailId;
//    }
//
//    public void setPromotionDetailId(int promotionDetailId) {
//        this.promotionDetailId = promotionDetailId;
//    }

//    public ArrayList<PromotionDetail> getPromotionDetail() {
//        return promotionDetail;
//    }
//
//    public void setPromotionDetail(ArrayList<PromotionDetail> promotionDetail) {
//        this.promotionDetail = promotionDetail;
//    }

    @Override
    public String toString() {
        return "ReceiptDetail{" + "id=" + id + ", productId=" + productId + ", productName=" + productName + ", productPrice=" + productPrice + ", productQty=" + productQty + ", productTotal=" + productTotal + ", receiptId=" + receiptId + '}';
    }

  

   
    
   
    public static ReceiptDetail fromRS(ResultSet rs) {
        ReceiptDetail receiptDetail = new ReceiptDetail();
        try {
            receiptDetail.setId(rs.getInt("receipt_detail_id"));
            receiptDetail.setProductId(rs.getInt("product_id"));
            receiptDetail.setProductName(rs.getString("product_name"));
            receiptDetail.setProductPrice(rs.getFloat("product_price"));
            receiptDetail.setProductQty(rs.getInt("product_qty"));
            receiptDetail.setProductTotal(rs.getFloat("product_total"));
            receiptDetail.setReceiptId(rs.getInt("receipt_id"));
           

        } catch (SQLException ex) {
            Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return receiptDetail;
    }
    
}
