/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HP
 */
public class Salary {

    private int id;
    private int employeeId;
    private int ownerId;
    private float payment;
    private Date date;

    public Salary(int id, int employeeId, int ownerId, float payment, Date date) {
        this.id = id;
        this.employeeId = employeeId;
        this.ownerId = ownerId;
        this.payment = payment;
        this.date = date;
    }

    public Salary(int employeeId, int ownerId, float payment, Date date) {
        this.id = -1;
        this.employeeId = employeeId;
        this.ownerId = ownerId;
        this.payment = payment;
        this.date = date;
    }

    public Salary() {
        this.id = -1;
        this.employeeId = 0;
        this.ownerId = 0;
        this.payment = 0;
        this.date = null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public float getPayment() {
        return payment;
    }

    public void setPayment(float payment) {
        this.payment = payment;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Salary{" + "id=" + id + ", employeeId=" + employeeId + ", ownerId=" + ownerId + ", payment=" + payment + ", date=" + date + '}';
    }
    
 

    public static Salary fromRS(ResultSet rs) {
        Salary salary = new Salary();
        try {
            salary.setId(rs.getInt("salary_id"));
            salary.setEmployeeId(rs.getInt("employee_id"));
            salary.setOwnerId(rs.getInt("owner_id"));
            salary.setPayment(rs.getFloat("salary_payment"));
            salary.setDate(rs.getTimestamp("salary_date"));
        } catch (SQLException ex) {
            Logger.getLogger(Salary.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return salary;
    }




}
