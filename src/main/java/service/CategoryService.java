/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.CategoryDao;
import java.util.List;
import model.Category;

/**
 *
 * @author informatics
 */
public class CategoryService {
    CategoryDao cd = new CategoryDao();
    public List<Category> getCategoryById(){
         
         return cd.getAll(" category_Id asc");
    }
    public Category addNew(Category editedCategory) {
       
        return cd.save(editedCategory);
    }

    public Category update(Category editedCategory) {
       
        return cd.update(editedCategory);
    }

    public int delete(Category editedCategory) {
        CategoryDao checkInOut = new CategoryDao();
        return checkInOut.delete(editedCategory);
    }
}
