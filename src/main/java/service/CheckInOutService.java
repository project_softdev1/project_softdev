/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.CheckInOutDao;
import java.util.List;
import model.CheckInOut;

/**
 *
 * @author Gift
 */
public class CheckInOutService {
    
    // have no login
    // asc by id not login
    public List<CheckInOut> getCheckInOuts(){
        CheckInOutDao checkInOut = new CheckInOutDao();
        return checkInOut.getAll(" checkinout_id asc");
    }

    public CheckInOut addNew(CheckInOut editedCheckInOut) {
        CheckInOutDao checkInOut = new CheckInOutDao();
        return checkInOut.save(editedCheckInOut);
    }

    public CheckInOut update(CheckInOut editedCheckInOut) {
        
        CheckInOutDao checkInOut = new CheckInOutDao();
        return checkInOut.update(editedCheckInOut);
    }

    public int delete(CheckInOut editedCheckInOut) {
        CheckInOutDao checkInOut = new CheckInOutDao();
        return checkInOut.delete(editedCheckInOut);
    }
}
