/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.CustomerDao;
import java.util.ArrayList;
import java.util.List;
import model.Customer;

/**
 *
 * @author Gift
 */
public class CustomerService {

    public static Customer currentCus;

    public Customer getByTel(String tel) {

        CustomerDao customerDao = new CustomerDao();
        
        Customer customer = customerDao.getByTel(tel);
        if (customer != null) {
            currentCus = customer;
            return customer;
        }
        return null;
    }
    public static Customer getCurrentCustomer(){
       return currentCus;
    }
    public List<Customer> getCustomers() {
        CustomerDao customerDao = new CustomerDao();
        return customerDao.getAll(" customer_id asc");
    }

    public Customer addNew(Customer editedCustomer) {
        CustomerDao customerDao = new CustomerDao();
        return customerDao.save(editedCustomer);
    }

    public Customer update(Customer editedCustomer) {
        CustomerDao customerDao = new CustomerDao();
        return customerDao.update(editedCustomer);
    }

    public int delete(Customer editedCustomer) {
        CustomerDao customerDao = new CustomerDao();
        return customerDao.delete(editedCustomer);

    }

}
