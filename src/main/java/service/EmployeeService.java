/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.EmployeeDao;
import java.util.ArrayList;
import java.util.List;
import model.Employee;

/**
 *
 * @author Gift
 */
  public class EmployeeService {

    public static Employee currentEmployee;

    public Employee login(String login, String password) {
        EmployeeDao employeeDao = new EmployeeDao();
        Employee employee = employeeDao.getByLogin(login);
        if (employee != null && employee.getPassword().equals(password)) {
            currentEmployee = employee;
            return currentEmployee;

        }
        return null;
    }

    public Employee getCurrentEmployee() {
//       System.out.println(currentEmployee);
        return currentEmployee;
    }

    public Employee getbyId(int id) {
        EmployeeDao employeeDao = new EmployeeDao();
        Employee employee = employeeDao.get(id);
        return employee;
    }

    // asc by id not login
    public List<Employee> getEmployees() {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.getAll(" employee_id asc");
    }

    public Employee addNew(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.save(editedEmployee);
    }

    public Employee update(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.update(editedEmployee);
    }

    public int delete(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.delete(editedEmployee);
    }

}
