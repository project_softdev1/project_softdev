/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import java.util.List;
import model.IngredientRecieptDetail;
import dao.IngredientRecieptDetailDao;

/**
 *
 * @author Asus Vivobook
 */
public class IngredientRecieptDetailService {

    public List<IngredientRecieptDetail> getIngredientRecieptDetail() {
        IngredientRecieptDetailDao ingredientRecieptDetailDao = new IngredientRecieptDetailDao();
        return ingredientRecieptDetailDao.getAll(" ingredient_reciept_id asc");
    }

    public IngredientRecieptDetail addNew(IngredientRecieptDetail editedIngredientRecieptDetail) {
        IngredientRecieptDetailDao ingredientDao = new IngredientRecieptDetailDao();
        return ingredientDao.save(editedIngredientRecieptDetail);
    }

    public IngredientRecieptDetail update(IngredientRecieptDetail editedIngredientRecieptDetail) {
        IngredientRecieptDetailDao ingredientDao = new IngredientRecieptDetailDao();
        return ingredientDao.update(editedIngredientRecieptDetail);
    }

    public int delete(IngredientRecieptDetail editedIngredientRecieptDetail) {
        IngredientRecieptDetailDao ingredientDao = new IngredientRecieptDetailDao();
        return ingredientDao.delete(editedIngredientRecieptDetail);
    }
}
