/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.IngredientDao;
import java.util.List;
import model.Ingredient;
import model.IngredientReciept;
import dao.IngredientRecieptDao;
import model.IngredientRecieptDetail;
import dao.IngredientRecieptDetailDao;

/**
 *
 * @author Asus Vivobook
 */
public class IngredientRecieptService {
    
    public List<IngredientReciept> getIngredientReciept(){
        IngredientRecieptDao ingredientRecieptDao = new IngredientRecieptDao();
            return ingredientRecieptDao.getAll(" ingredient_receipt_id asc");
                
    }
    public IngredientReciept addNew(IngredientReciept editedIngredientReciept) {
        IngredientRecieptDao ingredientDao = new IngredientRecieptDao();
        return ingredientDao.save(editedIngredientReciept);
    }

    public IngredientReciept update(IngredientReciept editedIngredientReciept) {
        IngredientRecieptDao ingredientDao = new IngredientRecieptDao();
        return ingredientDao.update(editedIngredientReciept);
    }

    public int delete(IngredientReciept editedIngredientReciept) {
        IngredientRecieptDao ingredientDao = new IngredientRecieptDao();
        return ingredientDao.delete(editedIngredientReciept);
    }
    
}
