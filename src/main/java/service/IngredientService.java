/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.IngredientDao;
import java.util.ArrayList;
import java.util.List;
import model.Ingredient;

/**
 *
 * @author paewnosuke
 */
public class IngredientService {
    private IngredientDao ingredientDao = new IngredientDao();
    public ArrayList<Ingredient> getIngredientOrderById(){
       return (ArrayList<Ingredient>) ingredientDao.getAll(" ingredient_id ASC");
    }
    public ArrayList<Ingredient> getIngredientOrderByQty(){
       return (ArrayList<Ingredient>) ingredientDao.getAll(" ingredient_qty ASC");
    }

    public List<Ingredient> getIngredients(){
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.getAll(" ingredient_id asc");
    }

    public Ingredient addNew(Ingredient editedIngredient) {
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.save(editedIngredient);
    }

    public Ingredient update(Ingredient editedIngredient) {
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.update(editedIngredient);
    }

    public int delete(Ingredient editedIngredient) {
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.delete(editedIngredient);
    }

}
