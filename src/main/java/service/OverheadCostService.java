/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.OverheadCostDao;
import java.util.List;
import model.OverheadCost;

/**
 *
 * @author Gift
 */
public class OverheadCostService {
    
    // have no login
    // asc by id not login
    public List<OverheadCost> getOverheadCosts(){
        OverheadCostDao overheadCost = new OverheadCostDao();
        return overheadCost.getAll(" overhead_cost_id asc");
    }

    public OverheadCost addNew(OverheadCost editedOverheadCost) {
        OverheadCostDao overheadCost = new OverheadCostDao();
        return overheadCost.save(editedOverheadCost);
    }

    public OverheadCost update(OverheadCost editedOverheadCost) {
        OverheadCostDao overheadCost = new OverheadCostDao();
        return overheadCost.update(editedOverheadCost);
    }

    public int delete(OverheadCost editedOverheadCost) {
        OverheadCostDao overheadCost = new OverheadCostDao();
        return overheadCost.delete(editedOverheadCost);
    }
}
