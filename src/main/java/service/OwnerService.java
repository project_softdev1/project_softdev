/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.OwnerDao;
import dao.OwnerDao;
import java.util.List;
import model.Owner;
import model.Owner;
import model.Owner;

/**
 *
 * @author Gift
 */
public class OwnerService {
    
    // have no login
    // asc by id not login
    
    public static Owner currentOwner;
    public Owner login(String login, String password) {
        OwnerDao ownerDao = new OwnerDao();
        Owner owner = ownerDao.getByLogin(login);
        if(owner != null && owner.getPassword().equals(password)) {
            currentOwner = owner;
            return currentOwner;
        }
        return null;
    }
    
    public Owner getCurrentOwner() {
        return currentOwner;
    }
    
    public List<Owner> getOwners(){
        OwnerDao ownerDao = new OwnerDao();
        return ownerDao.getAll(" owner_id asc");
    }

    public Owner addNew(Owner editedOwner) {
        OwnerDao ownerDao = new OwnerDao();
        return ownerDao.save(editedOwner);
    }

    public Owner update(Owner editedOwner) {
        OwnerDao ownerDao = new OwnerDao();
        return ownerDao.update(editedOwner);
    }

    public int delete(Owner editedOwner) {
        OwnerDao ownerDao = new OwnerDao();
        return ownerDao.delete(editedOwner);
    }
}
