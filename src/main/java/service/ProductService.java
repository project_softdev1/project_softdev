/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.ProductDao;
import java.util.ArrayList;
import java.util.List;
import model.Product;

/**
 *
 * @author Lenovo
 */
public class ProductService {

    private ProductDao productDao = new ProductDao();

    public ArrayList<Product> getProductOrderByName() {
        return (ArrayList<Product>) productDao.getAll(" product_name ASC");
    }


    public ArrayList<Product> getProductCoffee() {
        return (ArrayList<Product>) productDao.getAll(" category_id==1", " category_id ASC");
    }

    public Product addNew(Product editedProduct) {

        return productDao.save(editedProduct);
    }

    public Product update(Product editedProduct) {

        return productDao.update(editedProduct);
    }

    public int delete(Product editedProduct) {

        return productDao.delete(editedProduct);

    }
    public List<Product> getProducts() {
        ProductDao productDao = new ProductDao();
     return productDao.getAll(" product_id asc");
    }
    
  

    public ArrayList<Product> getProductDessert() {
        return (ArrayList<Product>) productDao.getAll(" category_id==2", " category_id ASC");
    }

    public ArrayList<Product> getProductMilk() {
        return (ArrayList<Product>) productDao.getAll(" category_id==3", " category_id ASC");
    }

    public ArrayList<Product> getProductTea() {
        return (ArrayList<Product>) productDao.getAll(" category_id==4", " category_id ASC");
    }

    public ArrayList<Product> getProductJuice() {
        return (ArrayList<Product>) productDao.getAll(" category_id==5", " category_id ASC");
    }

    public Product getById(int id) {
        return productDao.getById(id);
    }
}
