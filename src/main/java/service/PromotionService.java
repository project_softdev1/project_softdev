/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.PromotionDao;
import java.util.ArrayList;
//import dao.PromotionDetailDao;
import java.util.List;
import model.Promotion;
//import model.PromotionDetail;

/**
 *
 * @author Diarydear
 */
public class PromotionService {

    public static List<Promotion> getPro() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public ArrayList<Promotion> getPromotions() {
        PromotionDao promotion = new PromotionDao();
        return (ArrayList<Promotion>) promotion.getAll(" promotion_id asc");
    }

    public Promotion addNew(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
//        PromotionDetailDao pdd = new PromotionDetailDao();
//        Promotion promotion = promotionDao.save(editedPromotion);
//        for(PromotionDetail pd: editedPromotion.getPromotionDetail()){
//            pd.setPromotionId(promotion.getId());
//            pdd.save(pd);
//        }
        return promotionDao.save(editedPromotion);
    }

    public Promotion update(Promotion editedPromotion) {
        PromotionDao promotion = new PromotionDao();
        return promotion.update(editedPromotion);
    }

    public int delete(Promotion editedPromotion) {
        PromotionDao promotion = new PromotionDao();
        return promotion.delete(editedPromotion);
    }

    public ArrayList<Promotion> getCategory() {
        PromotionDao promotion = new PromotionDao();
        return (ArrayList<Promotion>) promotion.getProductCategory();
    }

}
