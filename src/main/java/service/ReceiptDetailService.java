/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.ProductReportDao;
import dao.ReceiptDetailDao;
import java.util.List;
import model.ProductReport;
import model.ReceiptDetail;

/**
 *
 * @author Lenovo
 */
public class ReceiptDetailService {
    public List<ReceiptDetail> getReceiptDetails(){
        ReceiptDetailDao receiptdetailDao = new ReceiptDetailDao();
        return receiptdetailDao.getAll(" receipt_detail_id asc");
    }

    public ReceiptDetail addNew(ReceiptDetail editedReceiptDetail) {
        ReceiptDetailDao receiptdetailDao = new ReceiptDetailDao();
        return receiptdetailDao.save(editedReceiptDetail);
    }

    public ReceiptDetail update(ReceiptDetail editedReceiptDetail) {
        ReceiptDetailDao receiptdetailDao = new ReceiptDetailDao();
        return receiptdetailDao.update(editedReceiptDetail);
    }

    public int delete(ReceiptDetail editedReceiptDetail) {
        ReceiptDetailDao receiptdetailDao = new ReceiptDetailDao();
        return receiptdetailDao.delete(editedReceiptDetail);
    }
    
    public List<ProductReport> getTopSellers() {
        ProductReportDao productReportDao = new ProductReportDao();
        return productReportDao.getTopSellers();
    }
    
    
}
