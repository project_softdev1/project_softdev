/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.ReceiptDao;
import dao.ReceiptDetailDao;
import java.util.List;
import model.Receipt;
import model.ReceiptDetail;

/**
 *
 * @author Lenovo
 */
public class ReceiptService {
      public Receipt getById(int id) {
         ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.getById(id);
    }

    public List<Receipt> getReceipts() {
         ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.getAll(" receipt_id asc");
    }
     public Receipt addNew(Receipt editedReceipt) {
        ReceiptDao receiptDao = new ReceiptDao();
        ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
        Receipt receipt = receiptDao.save(editedReceipt);
        for(ReceiptDetail rd: editedReceipt.getReceiptDetails()){
              rd.setReceiptId(receipt.getId());
              receiptDetailDao.save(rd);
        }
          return receipt;
    }
    public Receipt update(Receipt editedReceipt) {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.update(editedReceipt);
    }

    public int delete(Receipt editedReceipt) {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.delete(editedReceipt);
    }
}
