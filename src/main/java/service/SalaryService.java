/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.SalaryDao;
import java.util.List;
import model.Salary;

/**
 *
 * @author Gift
 */
public class SalaryService {
    

    public List<Salary> getSalarys(){
        SalaryDao salary = new SalaryDao();
        return salary.getAll(" salary_id asc");
    }

    public Salary addNew(Salary editedSalary) {
        SalaryDao salary = new SalaryDao();
        return salary.save(editedSalary);
    }

    public Salary update(Salary editedSalary) {
        SalaryDao salary = new SalaryDao();
        return salary.update(editedSalary);
    }

    public int delete(Salary editedSalary) {
        SalaryDao salary = new SalaryDao();
        return salary.delete(editedSalary);
    }
    
    public List<Salary> getSalaryByPayment() {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.getSalaryByPayment();
    }

    public Salary getByID(int id) {
        SalaryDao sd = new SalaryDao();
        Salary s = sd.getById(id);
        return s;
    }

}
