/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import dao.ProductDao;

import java.util.ArrayList;
import model.Promotion;
import service.PromotionService;
import java.util.Date;
import java.util.List;
import model.Product;

import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 *
 * @author Diarydear
 */
public class TestPromotionService {
    public static void main(String[] args) {
        PromotionService ps = new PromotionService();
        ps.getPromotions();
        for(Promotion promotion : ps.getPromotions()) {
            System.out.println(promotion);
        }
//       LocalDateTime currentDate = LocalDateTime.now();
//       Date date = Date.from(currentDate.atZone(ZoneId.systemDefault()).toInstant());
//        Promotion pro1 = new Promotion("New Discount", "Discount all 10%",date,date);
//        
//        ProductDao pd = new ProductDao();
//        List<Product> product = pd.getAll();
//        Product product0 = product.get(0);
//        Product product1 = product.get(1);
//        
//        PromotionDetailDao Prodd = new PromotionDetailDao();
//         ArrayList<PromotionDetail> pe1 =  (ArrayList<PromotionDetail>) Prodd.getAll();
//         PromotionDetail pd1 = pe1.get(0);
//        
//         PromotionDetail newDetail1 = new PromotionDetail( -1, (float) 0.5, 0);
//         pro1.addPromotionDetail(newDetail1);
//        
//        System.out.println(pro1);
//        for(Promotion promotion:ps.getPromotions()){
//           System.out.println(promotion);
//        }

    }
}
