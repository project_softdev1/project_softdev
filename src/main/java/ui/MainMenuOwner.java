/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package ui;

import ui.MainFrame;
import java.awt.Image;
import javax.swing.ImageIcon;
import model.Employee;
import model.Owner;
import service.EmployeeService;
import service.OwnerService;
import ui.CheckInOutFrame;

/**
 *
 * @author HP
 */
public class MainMenuOwner extends javax.swing.JFrame {

    private EmployeeService employeeService;
    private Employee currentEmployee;
    private OwnerService ownerService;

    /**
     * Creates new form MainMenu
     */
    public MainMenuOwner() {
        initComponents();
        showUser();
        setImage();
    }

    public void setImage() {

        ImageIcon icon1 = new ImageIcon("./Seller.png");
        Image newImage1 = setImageSize(icon1);
        icon1.setImage(newImage1);
        lblSellerImg.setIcon(icon1);
        lblSellerImg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        ImageIcon icon2 = new ImageIcon("./POS.png");
        Image newImage2 = setImageSize(icon2);
        icon2.setImage(newImage2);
        lblPOSImg.setIcon(icon2);
        lblPOSImg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        ImageIcon icon3 = new ImageIcon("./Stock.png");
        Image newImage3 = setImageSize(icon3);
        icon3.setImage(newImage3);
        lblStockImg.setIcon(icon3);
        lblStockImg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        ImageIcon icon4 = new ImageIcon("./Customer.png");
        Image newImage4 = setImageSize(icon4);
        icon4.setImage(newImage4);
        lblCusImg.setIcon(icon4);
        lblCusImg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        ImageIcon icon5 = new ImageIcon("./Order.png");
        Image newImage5 = setImageSize(icon5);
        icon5.setImage(newImage5);
        lblOrderImg.setIcon(icon5);
        lblOrderImg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        ImageIcon icon6 = new ImageIcon("./Employee.png");
        Image newImage6 = setImageSize(icon6);
        icon6.setImage(newImage6);
        lblEmpImg.setIcon(icon6);
        lblEmpImg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        ImageIcon icon7 = new ImageIcon("./Expenses.png");
        Image newImage7 = setImageSize(icon7);
        icon7.setImage(newImage7);
        lblExpensImg.setIcon(icon7);
        lblExpensImg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        ImageIcon icon8 = new ImageIcon("./Owner.png");
        Image newImage8 = setImageSize(icon8);
        icon8.setImage(newImage8);
        lblOwnerImg.setIcon(icon8);
        lblOwnerImg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        
        ImageIcon icon9 = new ImageIcon("./Product.png");
        Image newImage9 = setImageSize(icon9);
        icon9.setImage(newImage9);
        lblProductImg.setIcon(icon9);
        lblProductImg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        
        ImageIcon logo = new ImageIcon("./MainMenuWhite.png");
        Image newlogo = setLogoSize(logo);
        logo.setImage(newlogo);
        lblLogo.setIcon(logo);
        lblLogo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    }

    public Image setImageSize(ImageIcon icon) {
        Image image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        Image newImage = image.getScaledInstance((int) (100 * ((float) width) / height), 100, Image.SCALE_SMOOTH);
        return newImage;
    }
    
    public Image setLogoSize(ImageIcon icon) {
        Image image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        Image newImage = image.getScaledInstance((int) (25 * ((float) width) / height), 25, Image.SCALE_SMOOTH);
        return newImage;
    }

    public void showUser() {
        if (MainFrame.roleValue == "E") {
            employeeService = new EmployeeService();
            Employee currentEmployee = employeeService.getCurrentEmployee();
            if (currentEmployee != null) {
                // The currentEmployee is not null, so you can safely call its methods.
                String employeeName = currentEmployee.getName();
                lblShowName.setText(employeeService.getCurrentEmployee().getName());
                String r = " ";
                if (employeeService.getCurrentEmployee().getRole() == 0) {
                    r = "Admin";
                } else {
                    r = "User";
                }
                lblShowRole.setText(r);

                ImageIcon icon = new ImageIcon("./employee" + currentEmployee.getId() + ".png");
                Image newImage = setImageSize(icon);
                icon.setImage(newImage);
                lblPhotoLogin.setIcon(icon);
                lblPhotoLogin.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

            } else {
                System.out.println("error");
            }

        } else {
            ownerService = new OwnerService();
            Owner currentOwner = ownerService.getCurrentOwner();
            if (currentOwner != null) {
                // The currentEmployee is not null, so you can safely call its methods.
                String ownerName = currentOwner.getName();
                lblShowName.setText(ownerService.getCurrentOwner().getName());
                lblShowRole.setText("Owner");

                ImageIcon icon = new ImageIcon("./owner" + currentOwner.getId() + ".png");
                Image newImage = setImageSize(icon);
                icon.setImage(newImage);
                lblPhotoLogin.setIcon(icon);
                lblPhotoLogin.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

            } else {
                System.out.println("error");
            }
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        pnlSeller = new javax.swing.JPanel();
        lblSellerReport = new javax.swing.JLabel();
        lblSellerImg = new javax.swing.JLabel();
        pnlPOS = new javax.swing.JPanel();
        lblSellerReport1 = new javax.swing.JLabel();
        lblPOSImg = new javax.swing.JLabel();
        pnlStock = new javax.swing.JPanel();
        lblSellerReport2 = new javax.swing.JLabel();
        lblStockImg = new javax.swing.JLabel();
        pnlCustomer = new javax.swing.JPanel();
        lblSellerReport3 = new javax.swing.JLabel();
        lblCusImg = new javax.swing.JLabel();
        pnlOrder = new javax.swing.JPanel();
        lblSellerReport4 = new javax.swing.JLabel();
        lblOrderImg = new javax.swing.JLabel();
        pnlEmp = new javax.swing.JPanel();
        lblSellerReport5 = new javax.swing.JLabel();
        lblEmpImg = new javax.swing.JLabel();
        pnlExpenses = new javax.swing.JPanel();
        lblSellerReport6 = new javax.swing.JLabel();
        lblExpensImg = new javax.swing.JLabel();
        pnlOwner = new javax.swing.JPanel();
        lblSellerReport7 = new javax.swing.JLabel();
        lblOwnerImg = new javax.swing.JLabel();
        pnlProduct = new javax.swing.JPanel();
        lblSellerReport8 = new javax.swing.JLabel();
        lblProductImg = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        lblLogo = new javax.swing.JLabel();
        lblPhotoLogin = new javax.swing.JLabel();
        lblName = new javax.swing.JLabel();
        lblRole = new javax.swing.JLabel();
        lblShowName = new javax.swing.JLabel();
        lblShowRole = new javax.swing.JLabel();
        btnBlackMain = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Main Menu");
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
        });

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        jScrollPane1.setHorizontalScrollBar(null);

        jPanel1.setMaximumSize(new java.awt.Dimension(892, 548));

        jPanel2.setBackground(new java.awt.Color(232, 221, 204));

        pnlSeller.setBackground(new java.awt.Color(107, 76, 55));
        pnlSeller.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pnlSellerMouseClicked(evt);
            }
        });

        lblSellerReport.setBackground(new java.awt.Color(160, 104, 48));
        lblSellerReport.setFont(new java.awt.Font("TH SarabunPSK", 1, 18)); // NOI18N
        lblSellerReport.setForeground(new java.awt.Color(244, 238, 229));
        lblSellerReport.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSellerReport.setText("Seller Report");
        lblSellerReport.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblSellerReportMouseClicked(evt);
            }
        });

        lblSellerImg.setBackground(new java.awt.Color(193, 173, 153));
        lblSellerImg.setOpaque(true);

        javax.swing.GroupLayout pnlSellerLayout = new javax.swing.GroupLayout(pnlSeller);
        pnlSeller.setLayout(pnlSellerLayout);
        pnlSellerLayout.setHorizontalGroup(
            pnlSellerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSellerLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblSellerReport, javax.swing.GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(lblSellerImg, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlSellerLayout.setVerticalGroup(
            pnlSellerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlSellerLayout.createSequentialGroup()
                .addComponent(lblSellerImg, javax.swing.GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSellerReport, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pnlPOS.setBackground(new java.awt.Color(107, 76, 55));

        lblSellerReport1.setBackground(new java.awt.Color(160, 104, 48));
        lblSellerReport1.setFont(new java.awt.Font("TH SarabunPSK", 1, 18)); // NOI18N
        lblSellerReport1.setForeground(new java.awt.Color(244, 238, 229));
        lblSellerReport1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSellerReport1.setText("Point of Sell");

        lblPOSImg.setBackground(new java.awt.Color(193, 173, 153));
        lblPOSImg.setOpaque(true);
        lblPOSImg.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblPOSImgMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout pnlPOSLayout = new javax.swing.GroupLayout(pnlPOS);
        pnlPOS.setLayout(pnlPOSLayout);
        pnlPOSLayout.setHorizontalGroup(
            pnlPOSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPOSLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblSellerReport1, javax.swing.GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(lblPOSImg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlPOSLayout.setVerticalGroup(
            pnlPOSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlPOSLayout.createSequentialGroup()
                .addComponent(lblPOSImg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSellerReport1)
                .addContainerGap())
        );

        pnlStock.setBackground(new java.awt.Color(107, 76, 55));
        pnlStock.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pnlStockMouseClicked(evt);
            }
        });

        lblSellerReport2.setBackground(new java.awt.Color(160, 104, 48));
        lblSellerReport2.setFont(new java.awt.Font("TH SarabunPSK", 1, 18)); // NOI18N
        lblSellerReport2.setForeground(new java.awt.Color(244, 238, 229));
        lblSellerReport2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSellerReport2.setText("Stock Manegment");
        lblSellerReport2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblSellerReport2MouseClicked(evt);
            }
        });

        lblStockImg.setBackground(new java.awt.Color(193, 173, 153));
        lblStockImg.setOpaque(true);

        javax.swing.GroupLayout pnlStockLayout = new javax.swing.GroupLayout(pnlStock);
        pnlStock.setLayout(pnlStockLayout);
        pnlStockLayout.setHorizontalGroup(
            pnlStockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlStockLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblSellerReport2, javax.swing.GroupLayout.PREFERRED_SIZE, 128, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(lblStockImg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlStockLayout.setVerticalGroup(
            pnlStockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlStockLayout.createSequentialGroup()
                .addComponent(lblStockImg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSellerReport2)
                .addContainerGap())
        );

        pnlCustomer.setBackground(new java.awt.Color(107, 76, 55));
        pnlCustomer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pnlCustomerMouseClicked(evt);
            }
        });

        lblSellerReport3.setBackground(new java.awt.Color(160, 104, 48));
        lblSellerReport3.setFont(new java.awt.Font("TH SarabunPSK", 1, 18)); // NOI18N
        lblSellerReport3.setForeground(new java.awt.Color(244, 238, 229));
        lblSellerReport3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSellerReport3.setText("Customer Manegment");

        lblCusImg.setBackground(new java.awt.Color(193, 173, 153));
        lblCusImg.setOpaque(true);

        javax.swing.GroupLayout pnlCustomerLayout = new javax.swing.GroupLayout(pnlCustomer);
        pnlCustomer.setLayout(pnlCustomerLayout);
        pnlCustomerLayout.setHorizontalGroup(
            pnlCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblSellerReport3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addComponent(lblCusImg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlCustomerLayout.setVerticalGroup(
            pnlCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlCustomerLayout.createSequentialGroup()
                .addComponent(lblCusImg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSellerReport3, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pnlOrder.setBackground(new java.awt.Color(107, 76, 55));

        lblSellerReport4.setBackground(new java.awt.Color(160, 104, 48));
        lblSellerReport4.setFont(new java.awt.Font("TH SarabunPSK", 1, 18)); // NOI18N
        lblSellerReport4.setForeground(new java.awt.Color(244, 238, 229));
        lblSellerReport4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSellerReport4.setText("Order Manegment");

        lblOrderImg.setBackground(new java.awt.Color(193, 173, 153));
        lblOrderImg.setOpaque(true);
        lblOrderImg.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblOrderImgMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout pnlOrderLayout = new javax.swing.GroupLayout(pnlOrder);
        pnlOrder.setLayout(pnlOrderLayout);
        pnlOrderLayout.setHorizontalGroup(
            pnlOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrderLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblSellerReport4, javax.swing.GroupLayout.PREFERRED_SIZE, 128, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(lblOrderImg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlOrderLayout.setVerticalGroup(
            pnlOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlOrderLayout.createSequentialGroup()
                .addComponent(lblOrderImg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSellerReport4)
                .addContainerGap())
        );

        pnlEmp.setBackground(new java.awt.Color(107, 76, 55));
        pnlEmp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pnlEmpMouseClicked(evt);
            }
        });

        lblSellerReport5.setBackground(new java.awt.Color(160, 104, 48));
        lblSellerReport5.setFont(new java.awt.Font("TH SarabunPSK", 1, 18)); // NOI18N
        lblSellerReport5.setForeground(new java.awt.Color(244, 238, 229));
        lblSellerReport5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSellerReport5.setText("Employee Manegment");

        lblEmpImg.setBackground(new java.awt.Color(193, 173, 153));
        lblEmpImg.setOpaque(true);

        javax.swing.GroupLayout pnlEmpLayout = new javax.swing.GroupLayout(pnlEmp);
        pnlEmp.setLayout(pnlEmpLayout);
        pnlEmpLayout.setHorizontalGroup(
            pnlEmpLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblEmpImg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(lblSellerReport5, javax.swing.GroupLayout.PREFERRED_SIZE, 140, Short.MAX_VALUE)
        );
        pnlEmpLayout.setVerticalGroup(
            pnlEmpLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlEmpLayout.createSequentialGroup()
                .addComponent(lblEmpImg, javax.swing.GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSellerReport5)
                .addContainerGap())
        );

        pnlExpenses.setBackground(new java.awt.Color(107, 76, 55));
        pnlExpenses.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pnlExpensesMouseClicked(evt);
            }
        });

        lblSellerReport6.setBackground(new java.awt.Color(160, 104, 48));
        lblSellerReport6.setFont(new java.awt.Font("TH SarabunPSK", 1, 18)); // NOI18N
        lblSellerReport6.setForeground(new java.awt.Color(244, 238, 229));
        lblSellerReport6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSellerReport6.setText("Expenses of Store");

        lblExpensImg.setBackground(new java.awt.Color(193, 173, 153));
        lblExpensImg.setOpaque(true);

        javax.swing.GroupLayout pnlExpensesLayout = new javax.swing.GroupLayout(pnlExpenses);
        pnlExpenses.setLayout(pnlExpensesLayout);
        pnlExpensesLayout.setHorizontalGroup(
            pnlExpensesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlExpensesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblSellerReport6, javax.swing.GroupLayout.PREFERRED_SIZE, 128, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(lblExpensImg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlExpensesLayout.setVerticalGroup(
            pnlExpensesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlExpensesLayout.createSequentialGroup()
                .addComponent(lblExpensImg, javax.swing.GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSellerReport6)
                .addContainerGap())
        );

        pnlOwner.setBackground(new java.awt.Color(107, 76, 55));
        pnlOwner.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pnlOwnerMouseClicked(evt);
            }
        });

        lblSellerReport7.setBackground(new java.awt.Color(160, 104, 48));
        lblSellerReport7.setFont(new java.awt.Font("TH SarabunPSK", 1, 18)); // NOI18N
        lblSellerReport7.setForeground(new java.awt.Color(244, 238, 229));
        lblSellerReport7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSellerReport7.setText("Owner Manegment");

        lblOwnerImg.setBackground(new java.awt.Color(193, 173, 153));
        lblOwnerImg.setOpaque(true);

        javax.swing.GroupLayout pnlOwnerLayout = new javax.swing.GroupLayout(pnlOwner);
        pnlOwner.setLayout(pnlOwnerLayout);
        pnlOwnerLayout.setHorizontalGroup(
            pnlOwnerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblOwnerImg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(lblSellerReport7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlOwnerLayout.setVerticalGroup(
            pnlOwnerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlOwnerLayout.createSequentialGroup()
                .addComponent(lblOwnerImg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSellerReport7)
                .addContainerGap())
        );

        pnlProduct.setBackground(new java.awt.Color(107, 76, 55));
        pnlProduct.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pnlProductMouseClicked(evt);
            }
        });

        lblSellerReport8.setBackground(new java.awt.Color(160, 104, 48));
        lblSellerReport8.setFont(new java.awt.Font("TH SarabunPSK", 1, 18)); // NOI18N
        lblSellerReport8.setForeground(new java.awt.Color(244, 238, 229));
        lblSellerReport8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSellerReport8.setText("Product Manegment");
        lblSellerReport8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblSellerReport8MouseClicked(evt);
            }
        });

        lblProductImg.setBackground(new java.awt.Color(193, 173, 153));
        lblProductImg.setOpaque(true);

        javax.swing.GroupLayout pnlProductLayout = new javax.swing.GroupLayout(pnlProduct);
        pnlProduct.setLayout(pnlProductLayout);
        pnlProductLayout.setHorizontalGroup(
            pnlProductLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlProductLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblSellerReport8, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(lblProductImg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlProductLayout.setVerticalGroup(
            pnlProductLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlProductLayout.createSequentialGroup()
                .addComponent(lblProductImg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSellerReport8)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(92, 92, 92)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlExpenses, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlSeller, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlOrder, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(79, 79, 79)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(pnlStock, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlPOS, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlEmp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(67, 67, 67)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlCustomer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlOwner, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlProduct, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(69, 69, 69))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlSeller, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlPOS, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlCustomer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(27, 27, 27)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlEmp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlOrder, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlOwner, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(31, 31, 31)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlExpenses, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlStock, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlProduct, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18))
        );

        jPanel3.setBackground(new java.awt.Color(184, 164, 149));

        jPanel12.setBackground(new java.awt.Color(107, 76, 55));

        jLabel1.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(244, 238, 229));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Main Menu");

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1)
                    .addComponent(lblLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(28, Short.MAX_VALUE))
        );

        lblPhotoLogin.setBackground(new java.awt.Color(255, 255, 255));

        lblName.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        lblName.setForeground(new java.awt.Color(107, 76, 55));
        lblName.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblName.setText("Name:");

        lblRole.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        lblRole.setForeground(new java.awt.Color(107, 76, 55));
        lblRole.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblRole.setText("Role:");

        lblShowName.setBackground(new java.awt.Color(244, 238, 229));
        lblShowName.setFont(new java.awt.Font("TH SarabunPSK", 0, 20)); // NOI18N
        lblShowName.setForeground(new java.awt.Color(107, 76, 55));
        lblShowName.setOpaque(true);

        lblShowRole.setBackground(new java.awt.Color(244, 238, 229));
        lblShowRole.setFont(new java.awt.Font("TH SarabunPSK", 0, 20)); // NOI18N
        lblShowRole.setForeground(new java.awt.Color(107, 76, 55));
        lblShowRole.setOpaque(true);

        btnBlackMain.setBackground(new java.awt.Color(107, 76, 55));
        btnBlackMain.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        btnBlackMain.setForeground(new java.awt.Color(244, 238, 229));
        btnBlackMain.setText("LOGOUT");
        btnBlackMain.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBlackMainActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblName)
                                .addGap(18, 18, 18))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblRole)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblShowRole, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblShowName, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(lblPhotoLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnBlackMain, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(25, 25, 25))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblPhotoLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblName)
                    .addComponent(lblShowName, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblRole)
                    .addComponent(lblShowRole, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnBlackMain, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jScrollPane1.setViewportView(jPanel1);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 967, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 515, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnBackMainActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackMainActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnBackMainActionPerformed

    private void btnBlackMainActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBlackMainActionPerformed
        openLogin();
    }//GEN-LAST:event_btnBlackMainActionPerformed

    private void pnlEmpMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlEmpMouseClicked
        openEmployeeFrame();
    }//GEN-LAST:event_pnlEmpMouseClicked

    private void lblSellerReportMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblSellerReportMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_lblSellerReportMouseClicked

    private void pnlSellerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlSellerMouseClicked
        openSellerReport();
    }//GEN-LAST:event_pnlSellerMouseClicked

    private void lblSellerReport2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblSellerReport2MouseClicked

    }//GEN-LAST:event_lblSellerReport2MouseClicked

    private void pnlStockMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlStockMouseClicked
        openStockFrame();
    }//GEN-LAST:event_pnlStockMouseClicked

    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked

    }//GEN-LAST:event_formMouseClicked

    private void pnlCustomerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlCustomerMouseClicked
        openCustomerFrame();
    }//GEN-LAST:event_pnlCustomerMouseClicked

    private void pnlOwnerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlOwnerMouseClicked
        openOwnerFrame();
    }//GEN-LAST:event_pnlOwnerMouseClicked

    private void pnlExpensesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlExpensesMouseClicked
        openOverheadCostFrame();
    }//GEN-LAST:event_pnlExpensesMouseClicked

    private void lblPOSImgMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblPOSImgMouseClicked
        openPOS();
    }//GEN-LAST:event_lblPOSImgMouseClicked

    private void lblSellerReport8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblSellerReport8MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_lblSellerReport8MouseClicked

    private void pnlProductMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlProductMouseClicked
        openProductFrame();
    }//GEN-LAST:event_pnlProductMouseClicked

    private void lblOrderImgMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblOrderImgMouseClicked
       openOrderMangement();
    }//GEN-LAST:event_lblOrderImgMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainMenuOwner.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainMenuOwner.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainMenuOwner.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainMenuOwner.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainMenuOwner().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBlackMain;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblCusImg;
    private javax.swing.JLabel lblEmpImg;
    private javax.swing.JLabel lblExpensImg;
    private javax.swing.JLabel lblLogo;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblOrderImg;
    private javax.swing.JLabel lblOwnerImg;
    private javax.swing.JLabel lblPOSImg;
    private javax.swing.JLabel lblPhotoLogin;
    private javax.swing.JLabel lblProductImg;
    private javax.swing.JLabel lblRole;
    private javax.swing.JLabel lblSellerImg;
    private javax.swing.JLabel lblSellerReport;
    private javax.swing.JLabel lblSellerReport1;
    private javax.swing.JLabel lblSellerReport2;
    private javax.swing.JLabel lblSellerReport3;
    private javax.swing.JLabel lblSellerReport4;
    private javax.swing.JLabel lblSellerReport5;
    private javax.swing.JLabel lblSellerReport6;
    private javax.swing.JLabel lblSellerReport7;
    private javax.swing.JLabel lblSellerReport8;
    private javax.swing.JLabel lblShowName;
    private javax.swing.JLabel lblShowRole;
    private javax.swing.JLabel lblStockImg;
    private javax.swing.JPanel pnlCustomer;
    private javax.swing.JPanel pnlEmp;
    private javax.swing.JPanel pnlExpenses;
    private javax.swing.JPanel pnlOrder;
    private javax.swing.JPanel pnlOwner;
    private javax.swing.JPanel pnlPOS;
    private javax.swing.JPanel pnlProduct;
    private javax.swing.JPanel pnlSeller;
    private javax.swing.JPanel pnlStock;
    // End of variables declaration//GEN-END:variables

    private void openCheckInOut() {
        CheckInOutFrame checkInOutFrame = new CheckInOutFrame();
        checkInOutFrame.setVisible(true);
        dispose();

    }

    private void openStockFrame() {
        StockFrame ef = new StockFrame();
        ef.setVisible(true);
        dispose();

    }

    private void openEmployeeFrame() {
        EmployeeFrame ef = new EmployeeFrame();
        ef.setVisible(true);
        dispose();

    }

    private void openCustomerFrame() {
        CustomerFrame cf = new CustomerFrame();
        cf.setVisible(true);
        dispose();

    }

    private void openOwnerFrame() {
        OwnerFrame of = new OwnerFrame();
        of.setVisible(true);
        dispose();

    }

    private void openOverheadCostFrame() {
        OverheadCostFrame ocf = new OverheadCostFrame();
        ocf.setVisible(true);
        dispose();
    }

    private void openPOS(){
       mainframeTest mt = new mainframeTest();
       mt.setVisible(true);
       dispose();
    }

    private void openSellerReport() {
        ReportProdutFrame rpf = new ReportProdutFrame();
        rpf.setVisible(true);
        dispose();

    }

    private void openProductFrame() {
        ProductFrame pdf = new ProductFrame();
        pdf.setVisible(true);
        dispose(); 
    }

    private void openOrderMangement() {
       OrderManagementFrame of = new OrderManagementFrame();
       of.setVisible(true);
       dispose();
    }

    private void openLogin() {
        MainFrame mf = new MainFrame();
        mf.setVisible(true);
        dispose();
    }
}
