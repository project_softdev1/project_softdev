/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package ui;

import dao.ProductDao;
import dao.PromotionDao;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.management.StringValueExp;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.table.AbstractTableModel;
import model.Category;
import model.Customer;
import model.Employee;
import model.Owner;
import model.Product;
import model.Promotion;
import model.Receipt;
import model.ReceiptDetail;
import service.CustomerService;
import service.EmployeeService;
import service.OwnerService;
import service.ProductService;
import service.PromotionService;
import service.ReceiptService;
import ui.SearchMemberDialog;
import ui.component.BuyProductable;
import ui.component.ProductListPanel;

/**
 *
 * @author Lenovo
 */
public class POSPanel extends javax.swing.JPanel implements BuyProductable {

    ArrayList<Product> product;
    ProductService productservice = new ProductService();
    Receipt receipt;
    ArrayList<Promotion> promotion;
    float Discount = 0;
    Customer customer;
    EmployeeService emps = new EmployeeService();
    CustomerService cs = new CustomerService();
    OwnerService ownerService = new OwnerService();
    ReceiptService receiptService = new ReceiptService();
    private final ProductListPanel productListpanel;

    /**
     * Creates new form POSPanel
     */
    public POSPanel() {
        initComponents();
        receipt = new Receipt();
        showTimeUserAndStatus();
        Showlogo();
        if (MainFrame.roleValue.equals("E")) {
            Employee currentemp = emps.getCurrentEmployee();
            receipt.setEmployee(currentemp);
            receipt.setEmployeeId(currentemp.getId());
            lblUser.setText("User:" + currentemp.getName());
            if (String.valueOf(currentemp.getRole()).equals("1")) {
                lblStatus.setText("Status:" + " Admin");
            } else {
                lblStatus.setText("Status:" + " Employee");
            }
            ImageIcon icon = new ImageIcon("./employee" + currentemp.getId() + ".png");
            Image newImage = setImageSize(icon);
            icon.setImage(newImage);
            lblimage.setIcon(icon);
            lblimage.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        } else {
            Owner currentOwner = ownerService.getCurrentOwner();
            receipt.setOwner(currentOwner);
            receipt.setOwnerid(currentOwner.getId());
            lblUser.setText("User:" + ownerService.getCurrentOwner().getName());
            lblStatus.setText("Status:" + " Owner");
            ImageIcon icon = new ImageIcon("./Owner" + ownerService.getCurrentOwner().getId() + ".png");
            Image newImage = setImageSize(icon);
            icon.setImage(newImage);
            lblimage.setIcon(icon);
            lblimage.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        }

        tblReceiptDetail.setModel(new AbstractTableModel() {
            String[] header = {"Name", "Price", "Qty", "Total"};

            @Override
            public String getColumnName(int column) {
                return header[column];
            }

            @Override
            public int getRowCount() {
                return receipt.getReceiptDetails().size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<ReceiptDetail> receiptDetails = receipt.getReceiptDetails();
                ReceiptDetail receiptDetail = receiptDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return receiptDetail.getProductName();
                    case 1:
                        return receiptDetail.getProductPrice();
                    case 2:
                        return receiptDetail.getProductQty();
                    case 3:
                        return receiptDetail.getProductTotal();
                    default:
                        return "";

                }

            }
        });
        product = productservice.getProductOrderByName();
        productListpanel = new ProductListPanel();
        productListpanel.addOnBuyProduct(this);
        scrProductList.setViewportView(productListpanel);
    }
//    public void checkDiscount(){
//        PromotionDao promotionDao = new PromotionDao();
//        PromotionService ps = new PromotionService();
//        promotion = ps.getCategory();
//        ArrayList<ReceiptDetail> receiptDetails = receipt.getReceiptDetails();
//        for(int i =0;i<promotion.size();i++){
//            String cate = promotion.get(i).getProductCategory();
////            int[] category = parseCategoryValues(cate);
//        }
//
//        for(int i =0;i<product.size();i++){
//           int pro = receiptDetails.get(i).getProductId();
//           Product rePro = productservice.getById(pro);
//           int reCate = rePro.getCategoryId();
//        }
//    }
//    

    public void Showlogo() {
        ImageIcon icon = new ImageIcon("./DcoffeLogo" + ".png");
        Image newImage = setImageSize(icon);
        icon.setImage(newImage);
        lblShowImgaeDcoffee.setIcon(icon);
        lblShowImgaeDcoffee.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    }

    private int[] parseCategoryValues(String[] categoryText) {
        int[] intA = new int[categoryText.length];
        for (int i = 0; i < categoryText.length; i++) {
            intA[i] = Integer.parseInt(categoryText[i]);
        }

        return intA;
    }

    public Image setImageSize(ImageIcon icon) {
        Image image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        Image newImage = image.getScaledInstance((int) (100 * ((float) width) / height), 100, Image.SCALE_SMOOTH);
        return newImage;
    }

    private void checkP() {

        PromotionService ps = new PromotionService();
        promotion = ps.getPromotions();

        ArrayList<ReceiptDetail> receiptDetails = receipt.getReceiptDetails();
        ProductDao prodao = new ProductDao();
        for (Promotion pro : promotion) {
            Discount = 0;
            if (pro.getProductCategory() != null) {
                String[] o = pro.getProductCategory().split(",");
                int[] category = parseCategoryValues(o);
                for (ReceiptDetail r : receiptDetails) {
                    Product p = prodao.getById(r.getProductId());
                    if (p.getCategoryId() == category[0] || p.getCategoryId() == category[1]) {
                        if (r.getProductQty() >= 2) {
                            Discount = (float) (p.getPrice() * pro.getPromotionDiscount());
                            lblDiscount.setText("Discount: " + Discount);
                            lblTotal.setText("Total: " + (receipt.getTotal() - Discount));
                            receipt.setTotalDiscount(Discount);
                            break;
                        }
                    }
                }
            } else {
                Discount = (float) (receipt.getTotal() * pro.getPromotionDiscount());
                lblDiscount.setText("Discount: " + Discount);
                lblTotal.setText("Total: " + (receipt.getTotal() - Discount));
                receipt.setTotalDiscount(Discount);
            }
            receipt.setPromotionId(pro.getId());
        }
    }

    private void checkC() {

        PromotionService ps = new PromotionService();
        promotion = ps.getPromotions();

        ArrayList<ReceiptDetail> receiptDetails = receipt.getReceiptDetails();
        ProductDao prodao = new ProductDao();
        for (Promotion pro : promotion) {
            Discount = 0;
            if (pro.getProductCategory() != null) {
                String[] o = pro.getProductCategory().split(",");
                int[] category = parseCategoryValues(o);
                for (ReceiptDetail r : receiptDetails) {
                    Product p = prodao.getById(r.getProductId());
                    if (p.getCategoryId() == category[0] || p.getCategoryId() == category[1]) {
                        if (r.getProductQty() >= 2) {
                            Discount = (float) (p.getPrice() * pro.getPromotionDiscount());
                            float total = receipt.getTotal();
                            float cash = Float.parseFloat(txtCash.getText());
                            if (cash >= total) {
                                float change = (cash - (total - Discount));
                                lblChange.setText("Change : " + change);
                            } else {
                                lblChange.setText("Change : 0.0");
                                JOptionPane.showMessageDialog(this, "Cash is Lower than total", "Warning", JOptionPane.INFORMATION_MESSAGE);
                            }
                            break;
                        }
                    }
                }
            } else {

                float total = receipt.getTotal();
                float cash = Float.parseFloat(txtCash.getText());
                if (cash >= total) {
                    float change = (cash - (total - Discount));
                    lblChange.setText("Change : " + change);
                } else {
                    lblChange.setText("Change : 0.0");
                    JOptionPane.showMessageDialog(this, "Cash is Lower than total", "Warning", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        }
    }

    private void checkPromt() {

        PromotionService ps = new PromotionService();
        promotion = ps.getPromotions();

        ArrayList<ReceiptDetail> receiptDetails = receipt.getReceiptDetails();
        ProductDao prodao = new ProductDao();
        for (Promotion pro : promotion) {
            Discount = 0;
            if (pro.getProductCategory() != null) {
                String[] o = pro.getProductCategory().split(",");
                int[] category = parseCategoryValues(o);
                for (ReceiptDetail r : receiptDetails) {
                    Product p = prodao.getById(r.getProductId());
                    if (p.getCategoryId() == category[0] || p.getCategoryId() == category[1]) {
                        if (r.getProductQty() >= 2) {
                            Discount = (float) (p.getPrice() * pro.getPromotionDiscount());
                            float total = receipt.getTotal();
                            float change = total - Discount;
                            txtCash.setText("" + change);
                            lblChange.setText("Change : 0.0");
                            break;
                        }
                    }
                }
            } else {
                float total = receipt.getTotal();
                float change = total - Discount;
                txtCash.setText("" + change);
                lblChange.setText("Change : 0.0");

            }
        }
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        QrCodeDialog qr = new QrCodeDialog(frame, true);
        qr.setLocationRelativeTo(this);
        qr.setVisible(true);
    }

    public void showTimeUserAndStatus() {
        Timer timer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Date now = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                lblDate.setText("Date: " + sdf.format(now));
            }
        });
        timer.start();
    }

    private void clearReciept() {
        receipt = new Receipt();
//        reciept.setMember(MemberService.getCurrentMember());
        if (MainFrame.roleValue.equals("E")) {
            receipt.setEmployee(EmployeeService.currentEmployee);
        } else {
            receipt.setOwner(ownerService.getCurrentOwner());
        }
        refreshReciept();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblShowImgaeDcoffee = new javax.swing.JLabel();
        lblUser = new javax.swing.JLabel();
        lblStatus = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        lblDate = new javax.swing.JLabel();
        lblimage = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        lblTel = new javax.swing.JLabel();
        lblMemberName = new javax.swing.JLabel();
        lblPoint = new javax.swing.JLabel();
        lblSubTotal = new javax.swing.JLabel();
        lblDiscount = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        btnSearchMember = new javax.swing.JButton();
        btnRegister = new javax.swing.JButton();
        btnSConfirm = new javax.swing.JButton();
        lblCash = new javax.swing.JLabel();
        txtCash = new javax.swing.JTextField();
        btnCalculate = new javax.swing.JButton();
        btnPromtpay = new javax.swing.JButton();
        lblChange = new javax.swing.JLabel();
        btnCancel = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        MainMenu = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        scrProductList = new javax.swing.JScrollPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblReceiptDetail = new javax.swing.JTable();

        setBackground(new java.awt.Color(244, 238, 229));
        setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        setPreferredSize(new java.awt.Dimension(1108, 670));

        jPanel1.setBackground(new java.awt.Color(107, 76, 55));
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));

        lblShowImgaeDcoffee.setBackground(new java.awt.Color(107, 76, 55));
        lblShowImgaeDcoffee.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        lblShowImgaeDcoffee.setForeground(new java.awt.Color(255, 255, 255));
        lblShowImgaeDcoffee.setOpaque(true);

        lblUser.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        lblUser.setForeground(new java.awt.Color(255, 255, 255));
        lblUser.setText("User:");

        lblStatus.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        lblStatus.setForeground(new java.awt.Color(255, 255, 255));
        lblStatus.setText("Status:");

        jLabel1.setFont(new java.awt.Font("TH SarabunPSK", 1, 48)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Point of Sell");

        lblDate.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        lblDate.setForeground(new java.awt.Color(255, 255, 255));
        lblDate.setText("Date:");

        lblimage.setBackground(new java.awt.Color(255, 255, 255));
        lblimage.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        lblimage.setForeground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblShowImgaeDcoffee, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 260, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 366, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblimage, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblUser, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDate, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblUser)
                                .addGap(5, 5, 5)
                                .addComponent(lblStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblDate, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(lblimage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblShowImgaeDcoffee, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())))
        );

        jPanel4.setBackground(new java.awt.Color(244, 238, 229));

        lblTel.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        lblTel.setText("Tel:");

        lblMemberName.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        lblMemberName.setText("MemberName:");

        lblPoint.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        lblPoint.setText("Point:");

        lblSubTotal.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        lblSubTotal.setText("SubTotal:");

        lblDiscount.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        lblDiscount.setText("Discount:");

        lblTotal.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        lblTotal.setText("Total:");

        btnSearchMember.setBackground(new java.awt.Color(107, 76, 55));
        btnSearchMember.setFont(new java.awt.Font("TH SarabunPSK", 1, 16)); // NOI18N
        btnSearchMember.setText("Search Member");
        btnSearchMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchMemberActionPerformed(evt);
            }
        });

        btnRegister.setBackground(new java.awt.Color(107, 76, 55));
        btnRegister.setFont(new java.awt.Font("TH SarabunPSK", 1, 12)); // NOI18N
        btnRegister.setText("Register Member");
        btnRegister.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegisterActionPerformed(evt);
            }
        });

        btnSConfirm.setBackground(new java.awt.Color(187, 237, 148));
        btnSConfirm.setFont(new java.awt.Font("TH SarabunPSK", 1, 16)); // NOI18N
        btnSConfirm.setText("Confirm");
        btnSConfirm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSConfirmActionPerformed(evt);
            }
        });

        lblCash.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        lblCash.setText("Cash:");

        txtCash.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        txtCash.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCashActionPerformed(evt);
            }
        });

        btnCalculate.setBackground(new java.awt.Color(107, 76, 55));
        btnCalculate.setFont(new java.awt.Font("TH SarabunPSK", 1, 16)); // NOI18N
        btnCalculate.setText("Calculate");
        btnCalculate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalculateActionPerformed(evt);
            }
        });

        btnPromtpay.setBackground(new java.awt.Color(107, 76, 55));
        btnPromtpay.setFont(new java.awt.Font("TH SarabunPSK", 1, 16)); // NOI18N
        btnPromtpay.setText("Promptpay");
        btnPromtpay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPromtpayActionPerformed(evt);
            }
        });

        lblChange.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        lblChange.setText("Change:0.0");

        btnCancel.setBackground(new java.awt.Color(243, 113, 113));
        btnCancel.setFont(new java.awt.Font("TH SarabunPSK", 1, 16)); // NOI18N
        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(btnRegister, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSearchMember, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(lblMemberName, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTel, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblPoint, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(btnCalculate, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnPromtpay, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(lblCash)
                        .addGap(18, 18, 18)
                        .addComponent(txtCash, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(16, 16, 16))
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(lblSubTotal)
                        .addComponent(lblChange)
                        .addComponent(lblDiscount)
                        .addComponent(lblTotal)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addComponent(btnCancel)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(btnSConfirm))))
                .addGap(18, 18, 18))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSubTotal)
                    .addComponent(lblMemberName, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTel, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPoint, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblCash, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCash, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblChange, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnPromtpay, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCalculate, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnCancel)
                            .addComponent(btnSConfirm)
                            .addComponent(btnRegister)
                            .addComponent(btnSearchMember))))
                .addContainerGap())
        );

        jPanel5.setBackground(new java.awt.Color(244, 238, 229));
        jPanel5.setForeground(new java.awt.Color(244, 238, 229));

        MainMenu.setBackground(new java.awt.Color(107, 76, 55));
        MainMenu.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        MainMenu.setForeground(new java.awt.Color(244, 238, 229));
        MainMenu.setText("Main Menu");
        MainMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MainMenuActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("TH SarabunPSK", 1, 24)); // NOI18N
        jLabel2.setText("Product List");

        jButton1.setBackground(new java.awt.Color(107, 76, 55));
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setText("Promotion");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(MainMenu, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(MainMenu)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(244, 238, 229));

        scrProductList.setBackground(new java.awt.Color(244, 238, 229));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 586, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(scrProductList)
                    .addGap(3, 3, 3)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(scrProductList, javax.swing.GroupLayout.DEFAULT_SIZE, 481, Short.MAX_VALUE))
        );

        jScrollPane1.setBackground(new java.awt.Color(244, 238, 229));

        tblReceiptDetail.setBackground(new java.awt.Color(244, 238, 229));
        tblReceiptDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblReceiptDetail);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 325, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSearchMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchMemberActionPerformed
        openSearchMemberDialog();
        receipt.setCustomer(cs.getCurrentCustomer());

        lblMemberName.setText("MemberName:" + CustomerService.currentCus.getName());
        lblTel.setText("Tel:" + CustomerService.currentCus.getTel());
        lblPoint.setText("Point" + String.valueOf(CustomerService.currentCus.getPoint()));
        System.out.println("" + cs.getCurrentCustomer());
    }//GEN-LAST:event_btnSearchMemberActionPerformed

    private void btnRegisterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegisterActionPerformed
        openRgisterMemberDialog();
        receipt.setCustomer(cs.getCurrentCustomer());

        lblMemberName.setText("MemberName:" + cs.currentCus.getName());
        lblTel.setText("Tel:" + cs.currentCus.getTel());
        lblPoint.setText("Point" + String.valueOf(cs.currentCus.getPoint()));
        System.out.println("" + cs.getCurrentCustomer());
    }//GEN-LAST:event_btnRegisterActionPerformed

    private void btnSConfirmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSConfirmActionPerformed
        String cash = (txtCash.getText());
        if (cash.equals("") || cash.equals("0.0")) {
            JOptionPane.showMessageDialog(this, "Plaese Input Cash in Text Field", "Warning", JOptionPane.WARNING_MESSAGE);
        } else {
            int input = JOptionPane.showConfirmDialog(this, "Do you want to proceed?", "Select an Option...",
                    JOptionPane.YES_NO_OPTION, 1);
            if (input == 0) {
                CustomerService cs = new CustomerService();
                
                int point=0;
                receipt.setCash(Float.parseFloat(txtCash.getText()));

//                receipt.setEmployee(EmployeeService.currentEmployee);
//                receipt.setOwner(ownerService.getCurrentOwner());
                receiptService.addNew(receipt);
                if(cs.currentCus!=null){
                   Customer cos = cs.currentCus;
                   cos.setPoint(point+1);
                   cs.update(cos);
                }
                
                System.out.println("" + receipt);
                openshowReceiptDialog();
                clearReciept();
                txtCash.setText("");
                lblChange.setText("Change: 0.0");
                lblMemberName.setText("MemberName:");
                lblTel.setText("Tel:");
                lblPoint.setText("Point:");
                Customer customer = CustomerService.currentCus = new Customer();
                System.out.println(customer);
                System.out.println("Add into RecieptDetail");
            } else {
                System.out.println("Cancel");
            }
        }
    }//GEN-LAST:event_btnSConfirmActionPerformed

    private void MainMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MainMenuActionPerformed
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        JFrame POSPanel = frame;
        mainframeTest io = new mainframeTest();
        
        MainMenuEmployee mepm = new MainMenuEmployee();
        MainMenuOwner meOwner = new MainMenuOwner();

        if (MainFrame.roleValue != null && MainFrame.roleValue.equals("E")) {
            mepm.setVisible(true);
            POSPanel.dispose();
            io.dispose();
        } else {
            meOwner.setVisible(true);
            POSPanel.dispose();
            io.dispose();
        }
           
        

    }//GEN-LAST:event_MainMenuActionPerformed

    private void btnCalculateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalculateActionPerformed
        checkC();

    }//GEN-LAST:event_btnCalculateActionPerformed

    private void btnPromtpayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPromtpayActionPerformed
        checkPromt();
    }//GEN-LAST:event_btnPromtpayActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        clearReciept();
        txtCash.setText("");
        System.out.println("Clear Table");
        CustomerService customerService = new CustomerService();
        Customer cuscurrent = customerService.currentCus = new Customer();
        System.out.println(cuscurrent);
        lblMemberName.setText("MemberName:");
        lblTel.setText("Tel:");
        lblPoint.setText("Point:");
    }//GEN-LAST:event_btnCancelActionPerformed

    private void txtCashActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCashActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCashActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        PromotiomFrame pf = new PromotiomFrame();
        pf.setVisible(true);
        
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton MainMenu;
    private javax.swing.JButton btnCalculate;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnPromtpay;
    private javax.swing.JButton btnRegister;
    private javax.swing.JButton btnSConfirm;
    private javax.swing.JButton btnSearchMember;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblCash;
    private javax.swing.JLabel lblChange;
    private javax.swing.JLabel lblDate;
    private javax.swing.JLabel lblDiscount;
    private javax.swing.JLabel lblMemberName;
    private javax.swing.JLabel lblPoint;
    private javax.swing.JLabel lblShowImgaeDcoffee;
    private javax.swing.JLabel lblStatus;
    private javax.swing.JLabel lblSubTotal;
    private javax.swing.JLabel lblTel;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JLabel lblUser;
    private javax.swing.JLabel lblimage;
    private javax.swing.JScrollPane scrProductList;
    private javax.swing.JTable tblReceiptDetail;
    private javax.swing.JTextField txtCash;
    // End of variables declaration//GEN-END:variables

    @Override
    public void buy(Product product, int qty) {
        receipt.addReceiptDetail(product, qty);
        refreshReciept();
    }

    private void refreshReciept() {
        checkP();

        tblReceiptDetail.revalidate();
        tblReceiptDetail.repaint();

        lblSubTotal.setText("SubTotal: " + receipt.getTotal());

    }

    private void openSearchMemberDialog() {
        Customer cus = new Customer();
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        SearchMemberDialog smd = new SearchMemberDialog(frame, cus);
        smd.setLocationRelativeTo(this);
        smd.setVisible(true);

    }

    private void openRgisterMemberDialog() {
        Customer cus = new Customer();
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        CustomerDialog ctd = new CustomerDialog(frame, cus);
        ctd.setLocationRelativeTo(this);
        ctd.setVisible(true);

    }

    private void openshowReceiptDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        showReceiptDialog rd = new showReceiptDialog(frame, receipt);
        rd.setLocationRelativeTo(this);
        rd.setVisible(true);
    }
}
