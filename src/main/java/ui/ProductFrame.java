/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package ui;


import ui.ProductDialog;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import service.ProductService;
import service.CategoryService;
import model.Product;
import model.Category;
import model.Employee;
import model.Owner;
import service.EmployeeService;
import service.OwnerService;
import ui.MainFrame;

/**
 *
 * @author DELL
 */
public class ProductFrame extends javax.swing.JFrame {

    private final ProductService productService;
    private List<Product> list;
    private Product editedProduct;
    private int ProductEditedIndex = -1;
    private EmployeeService employeeService;
    private OwnerService ownerService;
    
    public ProductFrame() {
        initComponents();
        productService = new ProductService();
        showUser();
        showLogo();
        list = productService.getProducts();
        tblProduct.setRowHeight(100);
        tblProduct.setModel(new AbstractTableModel() {
            String[] columnNames = {"Product ID", "Name", "Price", "Type", "Category ID", "Sweet Level"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Product product = list.get(rowIndex);

                switch (columnIndex) {
                    case 0:
                        return product.getId();
                    case 1:
                        return product.getName();
                    case 2:
                        return product.getPrice();
                    case 3:
                        return product.getType();
                    case 4:
                        return product.getCategoryId();
                    case 5:
                        return product.getSweetLevel();
                    default:
                        return "Unknown";
                }
            }

        });
    }
    
        private void showUser() {
        if (MainFrame.roleValue == "E") {
            employeeService = new EmployeeService();
            Employee currentEmployee = employeeService.getCurrentEmployee();
            if (currentEmployee != null) {
                // The currentEmployee is not null, so you can safely call its methods.
                String employeeName = currentEmployee.getName();
                lblShowName.setText(employeeService.getCurrentEmployee().getName());
                String r = " ";
                if (employeeService.getCurrentEmployee().getRole() == 0) {
                    r = "Admin";
                } else {
                    r = "User";
                }
                lblShowRole.setText(r);

                ImageIcon icon = new ImageIcon("./employee" + currentEmployee.getId() + ".png");
                Image newImage = setImageSize(icon);
                icon.setImage(newImage);
                lblPhoto.setIcon(icon);
                lblPhoto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

            } else {
                System.out.println("error");
            }

        } else {
            ownerService = new OwnerService();
            Owner currentOwner = ownerService.getCurrentOwner();
            if (currentOwner != null) {
                // The currentEmployee is not null, so you can safely call its methods.
                String ownerName = currentOwner.getName();
                lblShowName.setText(ownerService.getCurrentOwner().getName());
                lblShowRole.setText("Owner");

                ImageIcon icon = new ImageIcon("./owner" + currentOwner.getId() + ".png");
                Image newImage = setImageSize(icon);
                icon.setImage(newImage);
                lblPhoto.setIcon(icon);
                lblPhoto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

            } else {
                System.out.println("error");
            }
        }
    }
    
    public Image setLogoSize(ImageIcon icon) {
        Image image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        Image newImage = image.getScaledInstance((int) (25 * ((float) width) / height), 25, Image.SCALE_SMOOTH);
        return newImage;
    }    
    
    private void openDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        ProductDialog productDialog = new ProductDialog(frame, editedProduct);
        productDialog.setLocationRelativeTo(this);
        productDialog.setVisible(true);
        productDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }
        });
    }
        
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        lblPM = new javax.swing.JLabel();
        lblLogo = new javax.swing.JLabel();
        lblPhoto = new javax.swing.JLabel();
        lblName = new javax.swing.JLabel();
        lblRole = new javax.swing.JLabel();
        btnBackMain = new javax.swing.JButton();
        lblShowName = new javax.swing.JLabel();
        lblShowRole = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        btnDelete = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblProduct = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel4.setBackground(new java.awt.Color(244, 238, 229));

        jPanel1.setBackground(new java.awt.Color(184, 164, 149));

        jPanel2.setBackground(new java.awt.Color(107, 76, 55));

        lblPM.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        lblPM.setForeground(new java.awt.Color(244, 238, 229));
        lblPM.setText("Product Management");

        lblLogo.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblPM)
                .addContainerGap(14, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblLogo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblPM, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE))
                .addContainerGap(28, Short.MAX_VALUE))
        );

        lblPhoto.setBackground(new java.awt.Color(255, 255, 255));
        lblPhoto.setVerifyInputWhenFocusTarget(false);

        lblName.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        lblName.setForeground(new java.awt.Color(107, 76, 55));
        lblName.setText("Name:");

        lblRole.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        lblRole.setForeground(new java.awt.Color(107, 76, 55));
        lblRole.setText("Role:");

        btnBackMain.setBackground(new java.awt.Color(107, 76, 55));
        btnBackMain.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        btnBackMain.setForeground(new java.awt.Color(244, 238, 229));
        btnBackMain.setText("Main Menu");
        btnBackMain.setBorder(null);
        btnBackMain.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackMainActionPerformed(evt);
            }
        });

        lblShowName.setBackground(new java.awt.Color(244, 238, 229));
        lblShowName.setForeground(new java.awt.Color(107, 76, 55));
        lblShowName.setOpaque(true);

        lblShowRole.setBackground(new java.awt.Color(244, 238, 229));
        lblShowRole.setForeground(new java.awt.Color(107, 76, 55));
        lblShowRole.setOpaque(true);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblPhoto, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblShowName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblRole)
                        .addGap(21, 21, 21)
                        .addComponent(lblShowRole, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(btnBackMain, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblPhoto, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblName)
                    .addComponent(lblShowName, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblShowRole, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblRole))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnBackMain, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26))
        );

        jPanel3.setBackground(new java.awt.Color(244, 238, 229));

        btnDelete.setBackground(new java.awt.Color(212, 120, 120));
        btnDelete.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        btnDelete.setForeground(new java.awt.Color(105, 78, 60));
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnEdit.setBackground(new java.awt.Color(251, 198, 119));
        btnEdit.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        btnEdit.setForeground(new java.awt.Color(105, 78, 60));
        btnEdit.setText("Edit");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnAdd.setBackground(new java.awt.Color(114, 170, 113));
        btnAdd.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        btnAdd.setForeground(new java.awt.Color(105, 78, 60));
        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAdd)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEdit)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDelete)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnDelete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEdit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAdd))
                .addContainerGap())
        );

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));

        tblProduct.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblProduct.setToolTipText("");
        tblProduct.setInheritsPopupMenu(true);
        tblProduct.setSelectionBackground(new java.awt.Color(144, 127, 127));
        jScrollPane1.setViewportView(tblProduct);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 699, Short.MAX_VALUE)))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 467, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int selectedIndex = tblProduct.getSelectedRow();

        if (selectedIndex >= 0) {
            editedProduct = list.get(selectedIndex);
            int input = JOptionPane.showConfirmDialog(this, "Do you want to proceed?", "Select an Option...",
                    JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                productService.delete(editedProduct);
            }
            refreshTable();
        }

    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        editedProduct = new Product();
        openDialog();
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        int selectedIndex = tblProduct.getSelectedRow();
        System.out.println(selectedIndex);
        if (selectedIndex >= 0) {
            editedProduct = list.get(selectedIndex);
            openDialog();
        }
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnBackMainActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackMainActionPerformed
        openMainMenuFrame();
    }//GEN-LAST:event_btnBackMainActionPerformed

    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ProductFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ProductFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ProductFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ProductFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ProductFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnBackMain;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblLogo;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblPM;
    private javax.swing.JLabel lblPhoto;
    private javax.swing.JLabel lblRole;
    private javax.swing.JLabel lblShowName;
    private javax.swing.JLabel lblShowRole;
    private javax.swing.JTable tblProduct;
    // End of variables declaration//GEN-END:variables

    private void refreshTable() {
        list = productService.getProducts();
        tblProduct.revalidate();
        tblProduct.repaint();
    }

    private void openMainMenuFrame() {
        if (MainFrame.roleValue=="E"){
        openMainMenuEmpFrame();
        }else{
        openMainMenuOwnFrame();
        }
    }
    
    private void openMainMenuOwnFrame() {
        MainMenuOwner mainMenu = new MainMenuOwner();
        mainMenu.setVisible(true);
        this.dispose();

    }
    
     private void openMainMenuEmpFrame() {
        MainMenuEmployee mme = new MainMenuEmployee();
        mme.setVisible(true);
        this.dispose();

    }
    
    private Image setImageSize(ImageIcon icon) {
        Image image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        Image newImage = image.getScaledInstance((int) (100 * ((float) width) / height), 100, Image.SCALE_SMOOTH);
        return newImage;
    }

    private void showLogo() {
        ImageIcon logo = new ImageIcon("./ProductWhite.png");
        Image newlogo = setLogoSize(logo);
        logo.setImage(newlogo);
        lblLogo.setIcon(logo);
        lblLogo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    }
}
