/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package ui;

import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import service.ProductService;
import service.CategoryService;
import model.Product;
import model.Category;
import model.Employee;
import model.Ingredient;
import model.Owner;
import service.EmployeeService;
import service.IngredientService;
import service.OwnerService;

/**
 *
 * @author DELL
 */
public class StockFrame extends javax.swing.JFrame {

    private List<Ingredient> list;
    private Product editedProduct;
    private int ProductEditedIndex = -1;
    private EmployeeService employeeService;
    private OwnerService ownerService;
    private final IngredientService ingredientService;

    public StockFrame() {
        initComponents();
        ingredientService = new IngredientService();
        list = ingredientService.getIngredients();
        showUser();
        tblIngredient.setRowHeight(100);
        tblIngredient.setModel(new AbstractTableModel() {
            String[] columnNames = {"ID", "Name", "Quantity", "Price"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Ingredient ingredient = list.get(rowIndex);

                switch (columnIndex) {
                    case 0:
                        return ingredient.getId();
                    case 1:
                        return ingredient.getName();
                    case 2:
                        return ingredient.getQty();
                    case 3:
                        return ingredient.getPrice();
                    default:
                        return "Unknown";
                }
            }

        });
    }

    private void showUser() {
        if (MainFrame.roleValue == "E") {
            employeeService = new EmployeeService();
            Employee currentEmployee = employeeService.getCurrentEmployee();
            if (currentEmployee != null) {
                // The currentEmployee is not null, so you can safely call its methods.
                String employeeName = currentEmployee.getName();
                lblShowName.setText(employeeService.getCurrentEmployee().getName());
                String r = " ";
                if (employeeService.getCurrentEmployee().getRole() == 0) {
                    r = "Admin";
                } else {
                    r = "User";
                }
                lblShowRole.setText(r);

                ImageIcon icon = new ImageIcon("./employee" + currentEmployee.getId() + ".png");
                Image newImage = setImageSize(icon);
                icon.setImage(newImage);
                lblPhoto.setIcon(icon);
                lblPhoto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

            } else {
                System.out.println("error");
            }

        } else {
            ownerService = new OwnerService();
            Owner currentOwner = ownerService.getCurrentOwner();
            if (currentOwner != null) {
                // The currentEmployee is not null, so you can safely call its methods.
                String ownerName = currentOwner.getName();
                lblShowName.setText(ownerService.getCurrentOwner().getName());
                lblShowRole.setText("Owner");

                ImageIcon icon = new ImageIcon("./owner" + currentOwner.getId() + ".png");
                Image newImage = setImageSize(icon);
                icon.setImage(newImage);
                lblPhoto.setIcon(icon);
                lblPhoto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

            } else {
                System.out.println("error");
            }
        }
    }

    private void openDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        ProductDialog productDialog = new ProductDialog(frame, editedProduct);
        productDialog.setLocationRelativeTo(this);
        productDialog.setVisible(true);
        productDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        lblPhoto = new javax.swing.JLabel();
        lblName = new javax.swing.JLabel();
        lblRole = new javax.swing.JLabel();
        lblShowRole = new javax.swing.JLabel();
        lblShowName = new javax.swing.JLabel();
        btnBackMain = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        btnCheckStock = new javax.swing.JButton();
        BtnBill = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblIngredient = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        jScrollPane1.setHorizontalScrollBar(null);

        jPanel2.setBackground(new java.awt.Color(193, 173, 153));

        jPanel4.setBackground(new java.awt.Color(107, 76, 55));
        jPanel4.setToolTipText("");
        jPanel4.setName(""); // NOI18N

        jLabel1.setFont(new java.awt.Font("TH SarabunPSK", 1, 22)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(242, 242, 242));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Stock Management");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(jLabel1)
                .addContainerGap(26, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(31, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(29, 29, 29))
        );

        lblName.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        lblName.setForeground(new java.awt.Color(107, 76, 55));
        lblName.setText("Name:");

        lblRole.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        lblRole.setForeground(new java.awt.Color(107, 76, 55));
        lblRole.setText("Role:");

        lblShowRole.setBackground(new java.awt.Color(244, 238, 229));
        lblShowRole.setForeground(new java.awt.Color(107, 76, 55));
        lblShowRole.setOpaque(true);

        lblShowName.setBackground(new java.awt.Color(244, 238, 229));
        lblShowName.setForeground(new java.awt.Color(107, 76, 55));
        lblShowName.setOpaque(true);

        btnBackMain.setBackground(new java.awt.Color(107, 76, 55));
        btnBackMain.setFont(new java.awt.Font("TH SarabunPSK", 1, 20)); // NOI18N
        btnBackMain.setForeground(new java.awt.Color(242, 242, 242));
        btnBackMain.setText("Main Menu");
        btnBackMain.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackMainActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblPhoto, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblName)
                            .addComponent(lblRole))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblShowName, javax.swing.GroupLayout.DEFAULT_SIZE, 81, Short.MAX_VALUE)
                            .addComponent(lblShowRole, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(btnBackMain, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblPhoto, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblShowName, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblName))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblShowRole, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblRole))
                .addGap(140, 140, 140)
                .addComponent(btnBackMain, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(232, 221, 204));

        btnCheckStock.setBackground(new java.awt.Color(100, 80, 67));
        btnCheckStock.setForeground(new java.awt.Color(244, 238, 229));
        btnCheckStock.setText("CheckStock");
        btnCheckStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckStockActionPerformed(evt);
            }
        });

        BtnBill.setBackground(new java.awt.Color(100, 80, 67));
        BtnBill.setForeground(new java.awt.Color(244, 238, 229));
        BtnBill.setText("Bill");
        BtnBill.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBillActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(BtnBill, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCheckStock, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnCheckStock, javax.swing.GroupLayout.DEFAULT_SIZE, 47, Short.MAX_VALUE)
                    .addComponent(BtnBill, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        tblIngredient.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblIngredient);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 742, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(44, 44, 44))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 439, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(152, 152, 152))
        );

        jScrollPane1.setViewportView(jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 938, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 512, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnBackMainActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackMainActionPerformed
        if (MainFrame.roleValue == "E") {
            openMainMenuEmpFrame();
        } else {
            openMainMenuOwnFrame();
        }
    }//GEN-LAST:event_btnBackMainActionPerformed

    private void btnCheckStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCheckStockActionPerformed
        openCheckStockFrame();
    }//GEN-LAST:event_btnCheckStockActionPerformed

    private void BtnBillActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBillActionPerformed
        openBillIngredient();
    }//GEN-LAST:event_BtnBillActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(StockFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(StockFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(StockFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(StockFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new StockFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnBill;
    private javax.swing.JButton btnBackMain;
    private javax.swing.JButton btnCheckStock;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblPhoto;
    private javax.swing.JLabel lblRole;
    private javax.swing.JLabel lblShowName;
    private javax.swing.JLabel lblShowRole;
    private javax.swing.JTable tblIngredient;
    // End of variables declaration//GEN-END:variables

    private void refreshTable() {
        list = ingredientService.getIngredients();
        tblIngredient.revalidate();
        tblIngredient.repaint();
    }

    private void openCheckStockFrame() {
        CheckStockFrame csf = new CheckStockFrame();
        csf.setVisible(true);
        this.dispose();

    }

    private void openMainMenuOwnFrame() {
        MainMenuOwner mainMenu = new MainMenuOwner();
        mainMenu.setVisible(true);
        this.dispose();

    }

    private void openMainMenuEmpFrame() {
        MainMenuEmployee mme = new MainMenuEmployee();
        mme.setVisible(true);
        this.dispose();

    }

    private Image setImageSize(ImageIcon icon) {
        Image image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        Image newImage = image.getScaledInstance((int) (100 * ((float) width) / height), 100, Image.SCALE_SMOOTH);
        return newImage;
    }

    private void openBillIngredient() {
        BillIngredient BillIngredient = new BillIngredient();
        BillIngredient.setVisible(true);
        this.dispose();
    }


    
}
